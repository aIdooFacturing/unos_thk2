<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
 
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	var loopFlag = null;
	var session = window.localStorage.getItem("auto_flag");
	if(session==null) window.localStorage.setItem("auto_flag", false);
	
	var flag = false;
	function stopLoop(){
		var flag = window.localStorage.getItem("auto_flag");
		
		if(flag=="true"){
			flag = "false"
		}else{
			flag = "true"
		}
		
		window.localStorage.setItem("auto_flag", flag);
		
		if(flag=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
		
	var canvas;
	var ctx;
	
	var handle = 0;
	var incycleColor = "#A3D800";
	var waitColor = "#FF9100";
	var alarmColor = "#EC1C24";
	var noConnColor = "#D7D7D7";
	
	var startHour, startMinute;
	
	
	function time(){
		//console.log("날짜 : " + getToday());
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getStartTime(){
		var url = ctxPath + "/chart/getStartTime.do";
		var param = "shopId=" + shopId;;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = Number(data.split("-")[0])
				startMinute = data.split("-")[1] * 10;
				
				//createMachine();
			}, error : function(e1,e2,e3){ 
				console.log(e1,e2,e3)
			}
		});	
	};
	
	$(function(){
		$("#sDate").val(getToday().substr(0,10))
		time()
		getStartTime();
		// main.do 접속시 두번째 카테고리 선택되던것 해결 : 첫번째=0 , 두번째=1
		createNav("monitor_nav",0);
		/* $("#home").click(function(){ location.href= "http://52.243.39.17:8080/aIdoo_exhibit/chart/index.do" }); */
	
		canvas = document.getElementById("canvas");
		ctx = canvas.getContext("2d");
		canvas.width = contentWidth;
		canvas.height = contentHeight;
		$("#canvas").css({
			"z-index" : -7,		
			"position" : "absolute",
			"top" : marginHeight,
			"left" : marginWidth
		});
		
		// 장비 그룹 나누는 칸들 ..
		//drawGroupDiv();
		//document.oncontextmenu = function() {stopLoop()}; 
		setDivPos();
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		if(session=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		chkBanner();
	});
	
		
	/* function drawCircle(){
		var url = "${ctxPath}/chart/drawCircle.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				inCycleMachine = 0;
				waitMachine = 0;
				alarmMachine = 0;
				powerOffMachine = 0;
				
				$(".machine, .name").remove();
				$(json).each(function(idx, data){
					var bg;
					if(data.isChg){
						if(data.chgTy=="O"){
							if(data.lastChartStatus=="IN-CYCLE"){
								bg = "incycle_o.png"; 
								inCycleMachine++;
							}else if(data.lastChartStatus=="WAIT"){
								bg = "wait_o.png"; 
								waitMachine++;
							}else if(data.lastChartStatus=="ALARM"){
								bg = "alarm_o.png"; 
								alarmMachine++;
							}	
						}else{
							if(data.lastChartStatus=="IN-CYCLE"){
								bg = "incycle_c.png"; 
								inCycleMachine++;
							}else if(data.lastChartStatus=="WAIT"){
								bg = "wait_c.png"; 
								waitMachine++;
							}else if(data.lastChartStatus=="ALARM"){
								bg = "alarm_c.png"; 
								alarmMachine++;
							}
						}
					}else{
						if(data.lastChartStatus=="IN-CYCLE"){
							bg = "incycle_n.png"; 
							inCycleMachine++;
						}else if(data.lastChartStatus=="WAIT"){
							bg = "wait_n.png"; 
							waitMachine++;
						}else if(data.lastChartStatus=="ALARM"){
							bg = "alarm_n.png"; 
							alarmMachine++;
						}
					}
					
					if(data.lastChartStatus=="NO-CONNECTION"){
						bg = "noConn.png";	
						powerOffMachine++;
					};
					
					if(data.noTarget==1){
						bg  = "noTarget.png";
					}
					
					var name = "<div class='name' + id='n" + data.id + "' style='color:white; font-size:" + getElSize(25) + "'>" + decode(data.dvcName).replace(/<br>/gi, "<br/>") + "</div>" ;
										
					var circle = document.createElement("img");
					circle.setAttribute("id", "m" + data.dvcId);
					circle.setAttribute("class", "machine");
					circle.setAttribute("src", "${ctxPath}/images/" + bg);
					
					circle.style.cssText = "position : absolute" + 
											"; z-index : 99999" + 
											"; width : " + getElSize(data.w) + 
											"; height : " + getElSize(data.w) + 
											"; top : " + getElSize(data.y) +
											"; left : " +  getElSize(data.x);
					
					$(circle).dblclick(function(){
						if(data.noTarget==1) return;
						if(data.dvcId!=0){
							window.localStorage.setItem("dvcId", data.dvcId);
							window.localStorage.setItem("name", data.dvcName);
							
							//location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
							location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
						};	
					});
					
					 if(String(data.type).indexOf("IO") == -1 && data.type != null){
						var wifi = document.createElement("img");
						wifi.setAttribute("src", ctxPath + "/images/wifi.png");
						wifi.setAttribute("class", "wifi");
						
						var top = (getElSize(data.y - 60));
						var left = (getElSize(data.x + 20));
						
						if(data.name=="HM1250W" || data.name=="DCM37100F" || data.name=="HF7P"){
							top += getElSize(100);
						}
						
						wifi.style.cssText = "position : absolute" + 
											"; width : " + getElSize(60) + "px" +
											"; height : " + getElSize(60) + "px" +
											"; background-color : white" +
											"; border-radius :50%" + 
											"; top : " + top + "px" + 
											"; left : " + left + "px";
						
						$("#svg_td").append(wifi)
					} 
					
					
					$("#svg_td").append(circle, name);
					
					$("#n" + data.id).css({
						"position" : "absolute",
						"text-align" : "center",
						"z-index" : "999",
						"top" : getElSize(data.y) + getElSize(100),
 					});
					
					$("#n" + data.id).css({
						"left" : getElSize(data.x) + (getElSize(data.w)/2) - ($("#n" + data.id).width()/2) 
 					});
				});
				
				$(".status_span").remove();
				var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
				$("#container").append(inCycleSpan);

				var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
				$("#container").append(waitSpan);

				var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
				$("#container").append(alarmSpan);

				var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
				$("#container").append(noConnSpan);

				var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
				$("#container").append(totalSpan);
				
				$("#inCycleSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "rgb(28,198,28)"
				});

				$("#inCycleSpan").css({
					"top" : $("#status_chart").offset().top + getElSize(50),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#inCycleSpan").width()/2)
				});
				
				$("#waitSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#FF9100",
				});
				
				$("#waitSpan").css({
					"left" : $("#status_chart").offset().left + $("#status_chart").width() - getElSize(50) - $("#waitSpan").height(),
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#waitSpan").height()/2),
				});
				

				$("#alarmSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "rgb(255, 0, 0)",
				});
				
				$("#alarmSpan").css({
					"top" : $("#status_chart").offset().top + $("#status_chart").height() - getElSize(50) - $("#alarmSpan").height(),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#alarmSpan").width()/2)
				});

				$("#noConnSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#CFD1D2"
				});
				
				$("#noConnSpan").css({
					"left" : $("#status_chart").offset().left + getElSize(50),
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#noConnSpan").height()/2),
				});

				$("#totalSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(40),
					"z-index" : 10,
					"color" : "#ffffff",
					"text-align" : "center"
				});

				$("#total_title").css({
					"font-size" : getElSize(45),
				});

				$("#totalSpan").css({
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#totalSpan").height()/2),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#totalSpan").width()/2)  
				});
				
				setTimeout(drawCircle, 5000)
			}
		});
	}; */
	
	var handle = 0;
	$(function(){
		$("#bg_img").css({
			"position" : "absolute",
			"width" : getElSize(1610),
			"height" : getElSize(966),
			"top" : getElSize(300),
			"left" : getElSize(1100),
		});
		
		$("#bg_img2").css({
			"position" : "absolute",
			"width" : getElSize(1355),
			"height" : getElSize(2690),
			"top" : getElSize(300),
			"left" : getElSize(2200),
		});
		
		$("#bg_img3").css({
			"position" : "absolute",
			"width" : getElSize(1240),
			"height" : getElSize(3060),
			"top" : getElSize(300),
			"left" : getElSize(2200),
		});
		//drawCircle();
		//createNav("monitor_nav",0);

	
		/* canvas = document.getElementById("canvas");
		ctx = canvas.getContext("2d");
		canvas.width = contentWidth;
		canvas.height = contentHeight; */
		
		/* $("#canvas").css({
			"z-index" : -7,
			"position" : "absolute",
			"top" : marginHeight,
			"left" : marginWidth
		}); */
		
		//drawGroupDiv();
		//document.oncontextmenu = function() {stopLoop()}; 
		//setDivPos();
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		/* if(session=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		}; */
		
		chkBanner();
	});
			
	function createMachine(){
		clearInterval(dateInterval);
		
		var url = ctxPath + "/svg/getMachineInfo.do";
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		var minute = date.getMinutes();
		
		var time = year + "-" + month + "-" + day;
		var today = $("#sDate").val();
		
		var target_time_n = (startHour - 12) * 60 + startMinute;
		var target_time_d = startHour * 60 + startMinute;
		var current_time = hour * 60 + minute;
		
		/* console.log(current_time)
		console.log(target_time_n)
		console.log(target_time_d) */
		
		var nd;
	
		var isToday = true;
		
		if(getToday().substr(0,10) != today){
			isToday = false;
		}
		
		console.log(isToday)
		var worker;
		var prdctRatio;
		if(current_time >= target_time_n && current_time <= target_time_d){
			nd = 2;
		}else{
			nd = 1;
		}
		
		var param = "shopId=" + shopId + 
					"&startDateTime=" + today + 
					"&nd=" + nd;
		
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function (data){
				inCycleMachine = 0;
				waitMachine = 0;
				alarmMachine = 0;
				powerOffMachine = 0;
				console.log("Machine is : ")
				console.log(json)
				var json = data.machineList;
				
				$(".box").remove();
				

				var machineStatus;
				var cntCyl;
				
				
				
				$(".status_span, #totalSpan").remove();
				$(".total").remove();
				var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
				$("#container").append(inCycleSpan);
				
				var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
				$("#container").append(waitSpan);
				
				var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
				$("#container").append(alarmSpan);
				
				var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
				$("#container").append(noConnSpan);
				
				var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
				$("#container").append(totalSpan);
				
				$("#inCycleSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#98d101",
					"top" : getElSize(1500) + marginHeight,
					"left" : getElSize(1205) + marginWidth
				});
				
				$("#waitSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#FF9100",
					"top" : getElSize(1640) + marginHeight,
					"left" : getElSize(1470) + marginWidth
				});
				
				$("#alarmSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#EC1C24",
					"top" : getElSize(1920) + marginHeight,
					"left" : getElSize(1205) + marginWidth
				});
				
				$("#noConnSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#CFD1D2",
					"top" : getElSize(1640) + marginHeight,
					"left" : getElSize(940) + marginWidth
				});
				
				$("#totalSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#ffffff",
					"top" : getElSize(1640) + marginHeight,
					"left" : getElSize(1200) + marginWidth,
					"text-align" : "center"
				});
				
				$("#total_title").css({
					"font-size" : getElSize(45),
				});
				
				dateInterval = setTimeout(createMachine, 5000);
			}
		});
	};
	
	
	function setCirclePos(id, x, y){
		var url = ctxPath + "/chart/setCirclePos.do";
		var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				
			}
		});
	}
	

	
	function startPageLoop(){
		loopFlag = setInterval(function(){
			location.href=ctxPath + "/chart/singleChartStatus.do";
		},1000*5);
	};
	
	var isToday = true;
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			//"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width(),
		})
		
		$("img").css({
			"display" : "inline"	
		});
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		/* $("#title").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(80),
			"top" : getElSize(100) + marginHeight,
			"left" : getElSize(1750)
		}); */
		
		console.log("lang : " + getParameterByName('lang'));
		
		if(getParameterByName('lang')=='ko'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		} 
		
		
		// 우측 상단 "GMT" 텍스트 부
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 3,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		// 상태 컨트롤러 위치 조정  
		$("#status_chart").css({
			"width" : getElSize(700),
			"position" : "absolute",
			"z-index" : 9,
			"display" : "block",
			"top" : getElSize(1400) + marginHeight,
			"left" : getElSize(700) + marginWidth
		});
		
		$("#svg").css({
			"width" : $("#table").width() - $("#svg_td").offset().left + marginWidth ,
			"height" : originHeight,
			"left" : $("#svg_td").offset().left,
			"position" :"absolute",
			"z-index" : 9
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		

		// 컨트롤러 우측 상태 가동 범례 위치 조정 
		$("#legend_table").css({
            "position" : "absolute",
            "z-index" : 999,
            "bottom" : getElSize(100),
            "left" : getElSize(1500)
        });
        
        $("#legend_table td").css({
            "color" : "white",
            "font-size" : getElSize(20)
        });
        
        $("#incycle_legend, #wait_legend, #alarm_legend, #noconn_legend, #condition_legend, #offset_legend").css({
            "border-radius" : "50%",
            "width" : getElSize(60),
            "height" : getElSize(60)
        });
        
        $("#offset_chg_legend, #cdt_chg_legend").css({
            "width" : getElSize(50) + "px"
        });
        
        /* $("#condition_legend").css({
            "border-radius" : "50%",
            "border" : getElSize(10) + "px solid #7100D8",
            "margin-left" : getElSize(40)
        });
        
        $("#offset_legend").css({
            "border-radius" : "50%",
            "border" : getElSize(10) + "px solid #0278FF",
            "margin-left" : getElSize(40)
        }); */
				
		
		$("#sDate").css({
			"position" : "absolute",
			"font-size" : getElSize(50) + "px"
		}).val(getToday().substr(0,10)).change(editTargetDate);
		
		$("#editDvc").css({
			"position": "absolute",
			"left" :getElSize(3574)+marginWidth,
			"top" : getElSize(285) + marginHeight , 
			"z-index" : 9999
		})
		$("#editDvc input").css({
			"font-size" : getElSize(50),
			"text-decoration": "none",
			"display": "inline-block",
			"padding-left": getElSize(15),
			"padding-right": getElSize(15),
			"line-height": getElSize(80) + "px",
			"cursor": "pointer",
		    "background-color": "#474747",
		    "color":"#EAEAEA",
		    "border-radius": "12px",
/* 			"height" : getElSize(100),
			"width" : getElSize(550) */
		})
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	function editTargetDate(){
		var now = new Date(); 
		var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		
		var selectedDate = $("#sDate").val();
		var s_year = selectedDate.substr(0,4);
		var s_month = selectedDate.substr(5,2);
		var s_day = selectedDate.substr(8,2);

		var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
		
		if(todayAtMidn.getTime()<specificDate.getTime()){
			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
			$("#sDate").val(getToday().substr(0,10));
			return;
		};
		
		createMachine();
		
		dateTimer = setTimeout(function(){
			$("#sDate").val(getToday().substr(0,10));
			createMachine();
			clearInterval(dateTimer)
		}, 1000 * 60); 
	};

	
	var dateTimer = null;
	/* function time(){
		//$("#time").html(getToday());
		
	//	$("#time").html("현재시간 : " + getToday() + ", 데이터수신시간 : " + maxTime)
		
		handle = requestAnimationFrame(time)
		
		 
		var hour = new Date().getHours();
		var minute = new Date().getMinutes();
		
	}; */
/*	
	function editMove(){
		var url = ctxPath + "/chart/getStartTime.do";
		location.href=ctxPath + "/chart/dashBoardMove.do";
	}
*/	
	
	var dateInterval = null;
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
<div id="svg"></div>
	<div id="time"></div>
	<div id="title"><spring:message code="layout"></spring:message></div>
	<div id="title_right"></div>
<!-- 	<div id="editDvc">
		<input type="button" value="위치 이동" onClick="editMove()">
	</div>
 -->	
	<img alt="" src="${ctxPath }/images/status_chart.png" id="status_chart">
	
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<!-- <input id="sDate" type="date"> -->
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			
			
			
			
			
			<Tr>
				<Td>
					<span class='nav_span'  ></span>
					<img alt="" src="${ctxPath }/images/selected_blue.png" class='menu_left' style="display: none">
				</Td>
				
				<td rowspan="10" id="svg_td">
				
					<div>
						<img alt="" src="${ctxPath }/images/DashBoard/Layout1_Road1-NO-CONNECTION.svg" id="bg_img">
						<img alt="" src="${ctxPath }/images/DashBoard/Layout1_Road2-NO-CONNECTION.svg" id="bg_img2">
						<img alt="" src="${ctxPath }/images/DashBoard/Layout_SetupStation-NO-CONNECTION.svg" id="bg_img3">
						<!-- <img alt="" src="/Device_Status/resources/upload/Layout1_Road1-NO-CONNECTION.svg" id="bg_img"  style="width:93%"> -->
					</div>
				
                    <table id="legend_table">
                        <Tr>
                            <Td>
                                <div id="incycle_legend" style="background-color: rgb(28,198,28)"></div>
                            </Td>
                            <td>
                                Cutting
                            </td>
                            <Td rowspan="2">
                                <!-- <div id="condition_legend"></div> -->
                                <%-- <img src="${ctxPath }/images/condition_chg.png" id="cdt_chg_legend"> --%>
                            </Td>
                            <td rowspan="2">
                            <!--     Changed<br>Condition -->
                            </td>
                        </Tr>
                        <tr>
                            <Td>
                                <div id="wait_legend" style="background-color: yellow"></div>
                            </Td>
                            <td>
                                Waiting
                            </td>
                        </tr>
                        <Tr>
                            <Td>
                                <div id="alarm_legend" style="background-color: #FF0000"></div>
                            </Td>
                            <td>
                                Alarm
                            </td>
                            <Td rowspan="2">
                                <!-- <div id="offset_legend" ></div> -->
                                <%-- <img src="${ctxPath }/images/offset_chg.png" id="offset_chg_legend"> --%>
                            </Td>
                            <td rowspan="2">
                            <!--     Changed<br>Offset -->
                            </td>
                        </Tr>
                        <tr>
                            <Td>
                                <!-- <div id="noconn_legend" style="background-color: #323435"></div> -->
                                <div id="noconn_legend" style="background-color: rgb(208,210,211)"></div>
                            </Td>
                            <td>
                                Power off
                            </td>
                        </tr>
                    </table>
                </td>
                
			</Tr>
			
			
			
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
  	<canvas id="canvas"></canvas>
	
	<div id="intro_back"></div>
 	<span id="intro"></span>
 

	<%-- <div id="title_main" class="title"><spring:message code="layout"></spring:message></div> --%>
</body>
</html>	