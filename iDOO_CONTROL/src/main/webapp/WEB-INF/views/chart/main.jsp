<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
	var login_lv = window.sessionStorage.getItem("lv");

	$(function(){
		setEl();
		/* var login = window.sessionStorage.getItem("login");
		var login_time = window.sessionStorage.getItem("login_time");
		var time = new Date().getTime();
		if(login!=null && (time - login_time) / 1000 < 60*20) */ 
		timer();
		bindEvt();
	});
	
	function bindEvt(){
		$("img").not("#grid, #aIdoo, .flag, #logo, #logo2, #enter, #logout").hover(function(){
			var id = $(this).attr("name");
			$(this).attr("src", "${ctxPath}/images/icons/" + id + "_select.png");
		}, function(){
			var id = $(this).attr("name");
			$(this).attr("src", "${ctxPath}/images/icons/" + id + ".png");
		})
		
	};
	
	var handle = 0;
	function timer(){
		$("#today").html(getToday());
		handle = requestAnimationFrame(timer);
	};
	
	function setEl(){
		$("#link").css({
			"color" : "white",
			"text-decoration" : "underline",
			"font-size" :getElSize(50),
			"font-weight" : "bolder",
			//"cursor" : "pointer",
			"position" : "absolute",
			"right" : getElSize(50),
			"bottom" : getElSize(150)
		}).click(function(){
			//location.href = "http://mtmes.doosanmachinetools.com/DULink/chart/main.do";
		});
		
		$("body").css({
			"margin" : 0,
			"padding" : 0,
			/* "background" : "url(" + ctxPath + "/images/home_bg.png)",
			"background-size" : "100%", */
			"background-color" : "black",
			"overflow" : "hidden"
		});
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
			"background-color" : "black ",
			"text-align" : "center"
		});
		
		$("#container").css({
			"margin-top" : (originHeight/2) - ($("#container").height()/2), 
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
		});
		
		$("#today").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#logo3").css({
			"margin-left" : getElSize(30)	
		});
		
		$("#iconTable img").css({
			"cursor" : "pointer",
			"height" : getElSize(450),
			//"margin-top" :getElSize(200)
		}).click(showMenu);
		
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.3,
			"position" : "absolute",
			"background-color" : "white",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#iconTable").css({
			"margin-top" : getElSize(250) + marginHeight	
		});
		
		$("#iconTable td span").css({
			"color" : "white",
			"font-size" : getElSize(90)
		});
		
		$("#iconTable td").css({
			"text-align" : "center",
			"padding-left" : getElSize(200),
			"padding-right" : getElSize(200),
			"padding-top" : getElSize(100),
			"padding-bottom" : getElSize(100),
		});
		
		
		$("#iconTable img, span").css({
			"-webkit-filter" : "blur(0px)"
		});
		
		
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(80) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#grid").css({
			"position" : "absolute",
			"width" : contentWidth * 0.8,
		});
		
		$("#grid").css({
			"left" : (window.innerWidth/2) - ($("#grid").width()/2),
			//"top" : getElSize(100) + marginHeight
			"top" : getElSize(100) + marginHeight + getElSize(250)
		});
		
		$("#iconTable tr:nth(0) td img").not("#grid").css({
			"margin-top" : getElSize(100)
		});
		//chkBanner();
		
		$("#aIdoo").css({
			"position" : "absolute",
			"width" : getElSize(300)
		});
		
		$("#aIdoo").css({
			"left" : (window.innerWidth/2) - ($("#aIdoo").width()/2),
			"bottom" : marginHeight + getElSize(50)
		});
	};
	
	function goPage(el){
		var url = $(el).attr("url");
		if(url=="url"){
			alert("준비 중입니다.");
			return;
		}
		
		if($(el).attr("disable")=="true"){
			alert("권한이 없습니다.");
			return;
		};
		
		if(url == "banner"){
			getBanner();
			closePanel();
			$("#bannerDiv").css({
				"z-index" : 9999,
				"opacity" : 1
			});
			panel_flag = false;
			return;
		};
		
		location.href = "${ctxPath}/chart/" + url; 
	};
	 
	function showMenu(){
		var id = this.id;
		
		if(id=="monitor"){
			location.href = "${ctxPath}/chart/main.do?lang=" + lang;
		}else if(id=="analysis"){
			//location.href = "${ctxPath}/chart/performanceAgainstGoal_chart.do";
			location.href = "/Performance_Report_Chart/index.do?lang=" + lang;
			
		}else if(id=="inven"){
			location.href = "${ctxPath}/chart/incomeStock.do?lang=" + lang;
		}else if(id=="maintenance"){
			//location.href = "${ctxPath}/chart/alarmReport.do";
			location.href = "/Alarm_Manager/index.do?lang=" + lang;
		}else if(id=="order"){
			location.href = "${ctxPath}/chart/addTarget.do?lang=" + lang;
		}else if(id=="quality"){
			location.href = "${ctxPath}/chart/checkPrdct.do?lang=" + lang;
		}else if(id=="tool"){
			location.href = "${ctxPath}/chart/toolManager.do?lang=" + lang;
		}else if(id=="kpi"){
			location.href = "${ctxPath}/chart/performanceAgainstGoal_chart_kpi.do?lang=" + lang;
		}else if(id=="program_manager"){
			//location.href = "${ctxPath}/chart/programFTP.do";
			location.href = "/Program_Manager/index.do?lang=" + lang;
		}else if(id=="configuration"){
			var user_id = window.sessionStorage.getItem("user_id");
			
			if(user_id != 'admin'){
				alert("관리자 아이디로 로그인하세요!");
			}else{
				location.href = "/Device_Status/index.do?lang=" + lang;
			}
		}else if(id=="adv_report"){
			window.open(
					"http://52.185.156.250/Reports/browse/",
				  '_blank' // <- This is what makes it open in a new window.
				);
		}else if(id=="ml"){
			window.open(
					"https://studio.azureml.net/",
				  '_blank' // <- This is what makes it open in a new window.
				);
		}else if("remote"){
			 var ws = new WebSocket("ws://localhost:3000", "echo-protocol");
	
			 // 연결이 수립되면 서버에 메시지를 전송한다
			  ws.onopen = function(event) {
			    ws.send("Client message: Hi!");
			  }
	
			  // 서버로 부터 메시지를 수신한다
			  ws.onmessage = function(event) {
			    console.log("Server message: ", event.data);
			  }
	
			  // error event handler
			  ws.onerror = function(event) {
			    console.log("Server error message: ", event.data);
			  }
			  
			  
		}
		
		
		
		
	};
	
	
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
	};

	
</script>
</head>
<body>	
	<div id="intro_back"></div>
	<span id="intro"></span>

	<div id="today"></div>
	<div id="title_right"></div>
	<a href=""><img alt="" src="${ctxPath }/images/aIdoo.png" id="aIdoo" ></a>
	<img alt="" src="${ctxPath }/images/grid.png" id="grid">
	
	<div id="container">
		<div id="panel">
			<table id="panel_table" width="100%">
			</table>
		</div>
		<Center>
			<table id="iconTable" style="width: 50%">
				<Tr>
					<td style="width: 50%">
						<img alt="" src="${ctxPath }/images/icons/Monitoring.png" id="monitor" name="Monitoring"><Br>		
					</td>
					<td style="width: 50%">
						<img alt="" src="${ctxPath }/images/icons/Analysis.png" id="analysis" name="Analysis"><Br>
					</td>
				</Tr>
				<tr>
					<%-- <td>
						<img alt="" src="${ctxPath }/images/icons/Tool Management.png" id="tool" name="Tool Management"><Br>
					</td> --%>
					<td>
						<img alt="" src="${ctxPath }/images/icons/Maintenance.png" id="maintenance" name="Maintenance"><Br>
					</td>
					
					<td>
						<img alt="" src="${ctxPath }/images/icons/Configuration.png" id="configuration" name="Configuration"><Br>
					</td>
				</tr>
			</table>	
		</Center>
		
		<!-- <span id="link">OLD</span> -->
	</div>
</body>
</html>	