<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	var handle = 0;
	
	function closeForm(){
		$("#insertForm").css({
			"z-index" : -999,
			"display" : "none"
		});
		closeCorver()
	}
	
	function addWorker(){
		var url = "${ctxPath}/chart/addWorker.do";
		var param = "id=" + $("#workerId").html() + 
					"&name=" + $("#name").val() + 
					"&pwd=" + $("#pwd").val() + 
					"&part=" + $("#part").val() + 
					"&email=" + $("#email").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success"){
					closeForm();
					getWorkerList();
					
				}
			}
		});
	};
	
	$(function(){
		createNav("mainten_nav", 3);
		getWorkerList();
		bindEvt2();
		$(".excel").click(csvSend);
		setEl();
		setDate();
		
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do"; });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function calcTimeDiff(start, end){
		var startHour = Number(start.substr(0,2));
		var startMinute = Number(start.substr(3,2));
		var endHour = Number(end.substr(0,2));
		var endMinute = Number(end.substr(3,2));
		
		var startTime = new Date($("#startDate_form").val() + "," + $("#startTime_form").val());
		var endTime = new Date($("#endDate_form").val() + "," + $("#endTime_form").val());
		
		var timeDiff = (endTime.getTime() - startTime.getTime()) / 1000 /60;
		
		return timeDiff;
	}
	
	
	function bindEvt2(){

	};
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	var csvOutput;
	function csvSend(){
		var sDate, eDate;
		
		sDate = $("#sDate").val();
		eDate = $("#eDate").val();
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			//"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(25),
			"padding" : getElSize(15),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2").css({
			"width" : getElSize(3800)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		
		$(".alarmTable td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(35),
			"border": getElSize(5) + "px solid black"
		});
		
		$("#wrapper").css({
			"height" :getElSize(1550),
			"width" : $(".right").width(),
			"overflow" : "auto"
		});
		
		$("#insertForm").css({
			"width" : getElSize(1500),
			"position" : "absolute",
			"z-index" : -999,
			"display" : "none"
		});
		
		
		$("#insert table td, #update table td").css({
			"color" : "white",
			"font-size" : getElSize(60),
			"padding" : getElSize(30),
			//"border" : getElSize(2) + "px solid black",
			"text-align" : "Center",
			"background-color" : "#323232"
		});
		
		$("#insertForm").css({
			"left" : (originWidth/2) - ($("#insertForm").width()/2),
			"top" : (originHeight/2) - ($("#insertForm").height()/2)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	
	var className = "";
	var classFlag = true;

	function getWorkerList(){
		classFlag = true;
		var url = "${ctxPath}/chart/getAllWorkerList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var tr = "<thead>" + 
							"<Tr style='background-color:#222222'>" +
								"<Td>" +
									"사번" +
								"</Td>" +
								"<Td>" +
									"이름" +
								"</Td>" +
								"<Td>" +
									"부서" +
								"</Td>" +
								"<Td>" +
									"Email" +
								"</Td>" +
							"</Tr></thead><tbody>";
							
				$(json).each(function(idx, data){
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						tr += "<tr class='contentTr " + className + "' id='tr" + data.id + "' onclick='showEditForm(\"" + data.id + "\")'>" +
									"<td >" + data.id + "</td>" +
									"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + decodeURIComponent(data.part).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.email + "</td>" + 
									/* "<td><input type='checkbox'> </td>" + */ 
									/* "<td><button onclick='showUpdateForm(" + data.id + ")'>수정</button></td>" + */
							"</tr>";					
					}
				});
				
				tr += "</tbody>";
				
				$(".alarmTable").html(tr).css({
					"font-size": getElSize(40),
				});
				
				//$("button, input[type='time']").css("font-size", getElSize(40));
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1650),
					"overflow" : "hidden"
				});

				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('#table2'), getElSize(1650));
				$("#wrapper div:last").css("overflow", "auto")
			}
		});
	};

	function showEditForm(id){
		var url = "${ctxPath}/chart/getWorkerInfoo.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log(data.pwd)
				$("#workerId").html(data.id);
				$("#name").val(data.name);
				$("#pwd").val(data.pwd);
				$("#part").val(data.part);
				$("#email").val(data.email);
				
				$("#insertForm").css({
					"z-index" : 999,
					"display" : "inline"
				});
				
				showCorver();
			}
		});
	};

	function showInsertForm(){
		$("input").val("")
		getNextWorkerId();
	};

	function getNextWorkerId(){
		var url = "${ctxPath}/chart/getNextWorkerId.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "text",
			success : function(data){
				$("#workerId").html(data);
				
				$("#insertForm").css({
					"z-index" : 999,
					"display" : "inline"
				});
				
				showCorver();
			}
		});
	};
	function showCorver(){
		$("#corver").css("z-index", 999);	
	};


	function bindEvt(){
		
		$("#insert #cancel, #update #cancel").click(closeInsertForm);
		
	};
	
	function closeInsertForm(){
		hideCorver();
		$("#insertForm, #updateForm").css("z-index",-9999);
		$("#updateForm #rcvCnt").css("color", " white");
		return false;
	};
	
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="insertForm">
		<form action="" id="insert">
			<Table style="width: 100%">
				<tr >
					<td class='table_title'>
						사번
					</td>
					<td id="workerId">
						
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						성명
					</td>
					<td>
						<input type="text" id="name">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						비밀번호
					</td>
					<td>
						<input type="password" id="pwd">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						부서
					</td>
					<td>
						<input type="text" id="part">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						이메일
					</td>
					<td>
						<input type="text" id="email">
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;"><button onclick="addWorker(); return false;">확인</button> <button onclick="closeForm(); return false;">취소</button> </td>
				</tr>
			</Table>
		</form>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table style="width: 100%">
						<Tr>
							<!-- <Td align="right"><button onclick="getIncomStock()">조회</button></Td> --><Td align="right"><button onclick="showInsertForm()">추가</button></Td>
						</Tr>
					</table>
					
					<div id="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table2">
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left '>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	