<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		date.setDate(date.getDate()-2)
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	$(function(){
		$("#insertForm input[type='text']").keyup(calcTotalCnt)
		getWorkerList();
		getNonOpTy();
		getDvcList();
		getPrdNo();
		
		setDate();

		createNav("order_nav", 1);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
		showInsertForm();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$(".wrapper").css({
			"width" : $(".menu_right").width(),
			"margin-top" : getElSize(120),
			"position" : "absolute"	
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(80),
			"padding" : getElSize(20),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(80),
			"margin" : getElSize(20)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getWorkerList(){
		var url = "${ctxPath}/chart/getWorkerList.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var option = "";
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				$("#worker_form").html(option);
			}
		});
	};
		
	function calcTotalCnt(){
		var cnt1 = Number($("#cnt1").val());
		var cnt2 = Number($("#cnt2").val());
		var cnt3 = Number($("#cnt3").val());
		var cnt4 = Number($("#cnt4").val());
		var cnt5 = Number($("#cnt5").val());
		
		var sum = cnt1 + cnt2 + cnt3 + cnt4 +cnt5;
		$("#totalCnt").val(sum);
	};

	function getNonOpTy(){
		var url = ctxPath + "/chart/getNonOpTy.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.nonOpTy).replace(/\+/gi, " ") + "</option>";
				});
				
				$("#nonOpTy_form").html(options);
			}
		});		
	};

	function getDvcList(){
		var url = ctxPath + "/chart/getDevieList.do"
		var param = "shopId=" + shopId + 
					"&prdNo=" + $("#prdNo_form").val(); 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				if(json.length==0) options += "<option>장비 없음</option>";
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
				});
				
				$("#dvcId_form").html(options);
			}
		});	
	};
	
	function getPrdNo(){
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>";
				});
				
				$("#group").html(option);
				$("#prdNo_form").html(option).change(getDvcList);
				
			}
		});
	};
	
	function showInsertForm(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day= addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		$("input[type='time']").val(hour + ":" + minute).change(function(){
			$("#time_form").html(calcTimeDiff($("#startTime_form").val(), $("#endTime_form").val()))	
		});
		
		$("#date_form").val(year + "-" + month + "-" + day);
		//showCorver();
		$("#insertForm").css("z-index",999);
		
		return false;
	};

	function addPrdCmpl(){
		var cnt1 = $("#cnt1").val();
		var cnt2 = $("#cnt2").val();
		var cnt3 = $("#cnt3").val();
		var cnt4 = $("#cnt4").val();
		var cnt5 = $("#cnt5").val();
		
		if(cnt1==""){
			cnt1 = "0";
		};
		
		if(cnt2==""){
			cnt2 = "0";
		};
		
		if(cnt3==""){
			cnt3 = "0";
		};
		
		if(cnt4==""){
			cnt4 = "0";
		};
		
		if(cnt5==""){
			cnt5 = "0";
		};
		
		var workTime = Number(($("#workTimeH").val() * 60)) + Number($("#workTimeM").val()); 
		var url = "${ctxPath}/chart/addPrdCmplCnt.do";
		var param = "prdNo=" + $("#prdNo_form").val() + 
					"&dvcId=" + $("#dvcId_form").val() + 
					"&worker=" + $("#worker_form").val() + 
					"&date=" + $("#date_form").val() + 
					"&lot1=" + $("#lot1").val() + 
					"&lot2=" + $("#lot2").val() +
					"&lot3=" + $("#lot3").val() +
					"&lot4=" + $("#lot4").val() +
					"&lot5=" + $("#lot5").val() +
					"&cnt1=" + cnt1 +
					"&cnt2=" + cnt2 +
					"&cnt3=" + cnt3 +
					"&cnt4=" + cnt4 +
					"&cnt5=" + cnt5 + 
					"&totalCnt=" + $("#totalCnt").val() + 
					"&ty=" + $("#ty").val() + 
					"&workTime=" + workTime;
		
		console.log(param)
		if($("#dvcId_form").val()==0){
			alert("장비를 선택하세요.");
			$("#dvcId_form").focus();
			
			return false;
		};
		
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					alert("저장 되었습니다.")
				}	
			}
		});
		
		return false;
	};

	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="insertForm">
						<table style="width: 100%">
							<Tr> 
								<Td class='table_title'  style="width: 20%" > <spring:message code="prdct_machine_line"></spring:message> </Td> <Td width="30%" colspan="2"><select id="prdNo_form"></select> </Td> <Td class='table_title'  width="20%"  > <spring:message code="device"></spring:message> </Td> <Td colspan="2"><select id="dvcId_form"></select><select id='ty'><option value="2"><spring:message code="day"></spring:message></option><option value="1"><spring:message code="night"></spring:message></option> </select> </Td>
							</Tr>
							<Tr> 
								<Td class='table_title' ><spring:message code="worker"></spring:message></Td> <Td colspan="2"><select id="worker_form"></select>  </Td> <Td class='table_title'  > <spring:message code="date_"></spring:message> </Td> <Td colspan="2"><input type="date" id="date_form"> </Td>
							</Tr>
							<Tr> 
								<Td class='table_title'  > LOT </Td> <Td style="text-align: center;" ><input type="text" size="4" id='lot1'> </Td> <Td style="text-align: center;"><input type="text" size="4" id='lot2'> </Td> 
								<td style="text-align: center;"><input type="text" size="4" id='lot3'> </Td> <Td style="text-align: center;"><input type="text" size="4" id='lot4'> </Td> <Td style="text-align: center;"><input type="text" size="4" id='lot5'></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'   > <spring:message code="count"></spring:message> </Td> <Td style="text-align: center;" ><input type="text" size="4" id='cnt1' style="text-align: right;"> </Td> <Td style="text-align: center;"><input type="text" size="4" id='cnt2' style="text-align: right;"> </Td> 
								<td style="text-align: center;"><input type="text" size="4" id='cnt3' style="text-align: right;"> </Td> <Td style="text-align: center;"><input type="text" size="4" id='cnt4' style="text-align: right;"> </Td> <Td style="text-align: center;"><input type="text" size="4" id='cnt5' style="text-align: right;"></Td>
							</Tr>
							<Tr>
								<Td class='table_title' ><spring:message code="total_cnt"></spring:message></Td>
								<Td   style="text-align: center; color: white;" colspan="2"> <input id="totalCnt" value="0" size="4"  style="text-align: right;"> </Td>
								<Td class='table_title' ><spring:message code="prd_time"></spring:message></Td> <Td colspan="2" style="color: white;"><input type="text" id="workTimeH" value="10" size="5"><spring:message code="time"></spring:message> <input type="text" id="workTimeM" value="30" size="5"><spring:message code="minute"></spring:message> </Td>
							</Tr>
							<Tr>
								<Td colspan="6" style="text-align: center;"><button onclick="addPrdCmpl()" ><spring:message code="save"></spring:message></button></Td>
							</Tr>
						</table> 
					</div>
				</td>
				
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	