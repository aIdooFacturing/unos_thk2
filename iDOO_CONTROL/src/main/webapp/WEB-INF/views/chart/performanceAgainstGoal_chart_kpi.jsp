<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getBanner(){
		var url = "${ctxPath}/chart/getBanner.do";
		var param = "shopId=" + shopId;
		

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				if(window.localStorage.getItem("banner")=="true"){
					$("#intro").html(data.msg).css({
						"color" : data.rgb,
						"right" : - window.innerWidth
					})
					
					$("#intro").html(data.msg).css({
						"right" : - $("#intro").width()
					})
				}
				
				bannerWidth = $("#intro").width() + getElSize(100); 
				$("#intro").width(bannerWidth);
				$("#banner").val(data.msg);
				$("#color").val(data.rgb);
				
				twinkle();
				bannerAnim();
			}
		});		
	}
	
	var twinkle_opct = false;
	function twinkle(){
		var opacity;
		if(twinkle_opct){
			opacity = 0;
		}else{
			opacity = 1;
		}
		$("#intro").css("opacity",opacity);
		
		twinkle_opct = !twinkle_opct;
		setTimeout(twinkle, 300)
	};
	
	var bannerWidth;
	function bannerAnim(){
		$("#intro").width(bannerWidth - getElSize(10));
		$("#intro").animate({
			"right" : window.innerWidth  - getElSize(100)
		},8000, function(){
			$("#intro").css("right" , - $("#intro").width())
			//$(this).remove();
			bannerAnim();
		});
	};
	
	function chkBanner(){
		if(window.localStorage.getItem("banner")=="true"){
			getBanner();
			$("#intro_back").css("display","block");
		}else{
			$("#intro").html("");
			$("#intro_back").css("display","none");
		}
	};
	
	function cancelBanner(){
		window.localStorage.setItem("banner", "false");
		$("#bannerDiv").css("z-index",-9);
		chkBanner();
	};
	
	function addBanner(){
		var url = "${ctxPath}/chart/addBanner.do";
		var param = "msg=" + encodeURIComponent($("#banner").val()) + 
					"&rgb=" + $("#color").val() + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				window.localStorage.setItem("banner", "true");
				location.reload();
				/* $("#bannerDiv").css("z-index",-9);
				chkBanner(); */
			}
		});
	};
	
	function getGroup(){
		var url = "${ctxPath}/chart/getGroup.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.group + "'>" + data.group + "</option>"; 
				});
				
				$("#group").html(option);
				
				getTableData("jig");
			}
		});
	};
	
	function getTableData(el){
		var sDate = $("#jig_sdate").val();
		var url = "${ctxPath}/chart/getPerformanceAgainstGoal.do";

		window.localStorage.setItem("jig_sDate", sDate);
		var param = "sDate=" + sDate + 
					"&eDate=" + sDate + " 23:59:59" +
					"&shopId=" + shopId + 
					"&group=" + $("#group").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				maxPage = json.length - maxBar;
				cPage = 1;
				wcName = [];
				
				capa = [];
				plan = [];
				perform = [];
				machinePerform = [];
				fault =	[];
				
				$(json).each(function(idx, data){
					wcName.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
					tmpWcName = wcName
					
					capa.push(Number(data.capa*2));
					plan.push(Number(data.tgCyl))
					perform.push(Number(data.cntCyl));
					machinePerform.push(Number(data.dvcCntCyl));
					fault.push(Number(data.faultCnt))
					
					tmpCapa = capa;
					tmpPlan = plan;
					tmpPerform = perform;
					tmpMachinePerform = machinePerform;
					tmpFault = fault;
				});
				//setEl();
				
				
				if(json.length > maxBar){
					reArrangeArray();
					$("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
					
				}; 

				chart("chart");
				addSeries();
			}
		});
	};
	
	var jigCsv;
	var wcCsv;
	var selected_dvc;
	var className = "";
	var classFlag = true;
	var maxBar = 10;
	
	var cBarPoint = 0;

	var tmpWcName = new Array();

	var tmpCapa = [];
	var tmpPlan = [];
	var tmpPerform = [];
	var tmpMachinePerform = [];
	var tmpFault = [];

	function reArrangeArray(){
		
		capa = [];
		plan = [];
		perform = [];
		machinePerform = [];
		fault =	[];
		wcName = [];
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
			wcName[i-cBarPoint] = tmpWcName[i];

			capa[i-cBarPoint] = tmpCapa[i];
			plan[i-cBarPoint] = tmpPlan[i];
			perform[i-cBarPoint] = tmpPerform[i];
			machinePerform[i-cBarPoint] = tmpMachinePerform[i];
			fault[i-cBarPoint] = tmpFault[i];
		};
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
			//wcList[i-cBarPoint] = tmpWcList[i];	
		};
		
		$("#controller font").html(addZero(String(cPage)) + " / " + Math.ceil((maxPage + maxBar) / 10));
		
	};
	var maxPage;

	var cPage = 1;
	function nextBarArray(){
		if(cBarPoint>=maxPage || maxPage<=0){
			alert("${end_of_chart}");
			return;
		};
		
		cPage++
		cBarPoint += maxBar;
		reArrangeArray();
		
		setEl();
		chart("chart");
		addSeries();
		
		$("#arrow_left").attr("src", ctxPath + "/images/left_en.png");
		if(Math.ceil((maxPage + maxBar) / 10)==cPage) $("#arrow_right").attr("src", ctxPath + "/images/right_dis.png");
	};

	function prevBarArray(){
		if(cBarPoint<=0){
			alert("${first_of_chart}");
			return;
		};
		
		cBarPoint -= maxBar;
		cPage--;
		reArrangeArray();
		
		setEl();
		chart("chart");
		addSeries();
		
		$("#arrow_right").attr("src", ctxPath + "/images/right_en.png");
		if(cBarPoint==0) $("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
	};


	var capa = [];
	var plan = [];
	var perform = [];
	var machinePerform = [];
	var fault =	[];
	
	var dvc;
	var wcName = []
	function chart(id){
		$('#' + id).highcharts({
	        chart: {
	            type: 'column',
	            backgroundColor : 'rgb(50, 50, 50)',
	            height : getElSize(1600),
	        },
	        title: {
	            text:false
	        },
	        xAxis: {
	            categories: wcName,
	            labels : {
	            	style : {
	            		"color" : "white",
	            		"font-size" : getElSize(40)
	            	}
	            }
	        },
	        yAxis: {
	            min: 0,
	            //max : 20,
	            title: {
	                text: false
	            },
	            labels : {
	            	style : {
	            		"color" : "white",
	            		"font-size" : getElSize(30)
	            	},
	            	enabled : true
	            },
	            //reversed:true 
	        },
	        tooltip: {
	            enabled : false
	        },
	        plotOptions: {
	            column: {
	                dataLabels: {
	                    enabled: true,
	                    color: "white",
	                    style: {
	                    	fontSize : getElSize(40),
	                        textShadow: false
	                    }
	                }
	            },
	            series:{
	            	 pointWidth: getElSize(50)
	            }
	        },
	        credits : false,
	        exporting : false,
	        legend:{
	        	enabled : true,
	        	itemStyle: {
	                color: 'white',
	                fontSize : getElSize(40)
	             }, 
	             align: 'right',
	             verticalAlign: 'bottom'
	        },
	        series: []
	    });
		
		barChart = $("#" + id).highcharts();
	}
	
	function addSeries(){
		barChart.addSeries({
			color : "#7F3F97",
			data : capa,
			name : "Capa"
		}, true);
		
		barChart.addSeries({
			color : "#0080FF",
			data : plan,
			name : "${plan}"
		}, true);
		
		barChart.addSeries({
			color : "#A3D800",
			data : machinePerform,
			name : "${machine_performance}"
		}, true);
		
		barChart.addSeries({
			color : "#FAAF40",
			data : perform,
			name : "${worker_performance}"
		}, true);
		
		barChart.addSeries({
			color : "#EC1C24",
			data : fault,
			name : "${faulty}"
		}, true);
	}
	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	$(function(){
		createNav("kpi_nav", 0)
		getGroup();
		setDate();	
		time();
		
		$("#home").click(function(){ location.href = "/iDOO_CONTROL/chart/index.do" });
		setEl();
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#chart").css({
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			"margin-top" : getElSize(20),
		});
		
		$("#arrow_left").css({
			"width" : getElSize(60),
			"margin-right" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#arrow_right").css({
			"width" : getElSize(60),
			"margin-left" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#controller").css({
			"display" : "table",
			"background-color" : "black",
			"border-radius" : getElSize(70),
			"position" : "absolute",
			"bottom" : getElSize(65),
			"padding" : getElSize(15),
			//"width" : getElSize(600),
			"vertical-align" : "middle"
		});
		
		$("#controller").css({
			"left" : $("#content_table").offset().left - marginWidth + ($("#content_table").width()/2) - ($("#controller").width()/2)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		//location.href = "${ctxPath}/chart/"
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/kpi_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span  class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/selected_green.png" class='menu_left' >
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td><select id="group"></select>
								<spring:message code="op_period"></spring:message> 
								<input type="date" class="date" id="jig_sdate">
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getTableData()">
								<button onclick="goGraph()"><spring:message code="table"></spring:message></button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="chart"></div>
								<div id="controller">
									<img alt="" src="${ctxPath }/images/left_en.png" id="arrow_left"  onclick="prevBarArray();">
									<font style="display: table-cell; vertical-align:middle">01 / 00</font>
									<img alt="" src="${ctxPath }/images/right_en.png" id="arrow_right" onclick="nextBarArray();">
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'   >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left '  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	