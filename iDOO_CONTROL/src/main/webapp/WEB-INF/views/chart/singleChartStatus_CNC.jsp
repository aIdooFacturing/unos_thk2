<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/solid-gauge.js"></script>
<script src="${ctxPath }/js/chart/multicolor_series.js"></script>

<script type="text/javascript">
	var targetWidth = 3840;
	var targetHeight = 2160;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};

	var status1, status2;
	
	var today;
	var flag = false;
	
	var loopFlag = null;

	var session = window.sessionStorage.getItem("auto_flag");
	if(session==null) window.sessionStorage.setItem("auto_flag", true);

	function startPageLoop(){
		loopFlag = setInterval(function(){
			//location.href = ctxPath + "/chart/multiVision.do";
			location.href=  ctxPath + "/chart/main.do";
			
		},1000*10);
	};

	function stopLoop(){
		var flag = window.sessionStorage.getItem("auto_flag");
		
		if(flag=="true"){
			flag = "false"
		}else{
			flag = "true"
		}
		
		window.sessionStorage.setItem("auto_flag", flag);
		
		if(flag=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : 0
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			url =  "${ctxPath}/chart/main.do";
			location.href = url;
		}else if(type=="menu1"){
			url =  "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else if(type=="menu2"){
			url =  "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		}else if(type=="menu9"){
			url = "${ctxPath}/chart/singleChartStatus.do";
			location.href = url;
		}else if(type=="menu3"){
			url = "${ctxPath}/chart/main3.do";
			location.href = url;
		}else if(type=="menu4"){
			url = "${ctxPath}/chart/tableChart.do";
			location.href = url;
		}
	};
	
	function upDate(){
		var $date = $("#sdate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sdate").val(today);
		changeDateVal();
	};
	
	function downDate(){
		var $date = $("#sdate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sdate").val(today);
		changeDateVal();
	};
	
	function changeDateVal(){
		var chart = $("#status1").highcharts(); 
		var len = chart.series.length;
		
		
		
		var now = new Date(); 
		var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		
		var date = new Date();
		
		var year = date.getFullYear();
		var month = date.getMonth()+1;
		var day = date.getDate();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		// Set specificDate to a specified date at midnight.
		
		var selectedDate = $("#sdate").val();
		var s_year = selectedDate.substr(0,4);
		var s_month = selectedDate.substr(5,2);
		var s_day = selectedDate.substr(8,2);

		var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
		
		var time = year + "-" + month + "-" + day;

		if(todayAtMidn.getTime()<specificDate.getTime()){
			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth()+1));
			var day = addZero(String(date.getDate()));
			var hour = addZero(String(date.getHours()));
			var minute = addZero(String(date.getMinutes()));
			var second = addZero(String(date.getSeconds())); 
			
			today = year + "-" + month + "-" + day;
			$("#sdate").val(today);
			return;
		};
		
		if(today!=$("#sdate").val()){
			time = $("#sdate").val() + "%20" + workStartTime + ":59:59";
		}else{
			time = $("#sdate").val() + "%20" + hour + ":" + minute + ":" + second;
		}
		targetDate = time;

		for(var i = 0; i < len; i++){
			chart.series[i].remove(true)		
		};
		
		eval("dvcMap1").put("flag", true);
		eval("dvcMap1").put("currentFlag", true);
		
					
		getDvcStatus(dvcArray[0], 1);
	};
	
	var shopId = 1;

	function getDvcList(){
		var url = "${ctxPath}/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&jig=CNC";
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "";
				
				$(json).each(function(idx, data){
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});
	
				$("#dvcName1").html(list);
			}
		});
	};

	
	function getSpdFeed(){
		$('#spdFeed').highcharts({
	        chart: {
	            type: 'line',
	            backgroundColor : "rgba(0,0,0,0)",
	            height : getElSize(500),
				width : getElSize(3550),
	        },
	        exporting : false,
	        credits : false,
	        title: {
	            text: null
	        },
	        subtitle: {
	            text: null
	        },
	        legend : {
	        	itemStyle : {
	        			color : "white",
	        			fontWeight: 'bold'
	        		}
	        },
	        xAxis: {
	            //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	        	labels : {
	        		enabled : false
	        	}
	        },
	        yAxis: {
	        	min : 0,
	            title: {
	                text: null
	            },
	            labels : {
	            	style : {
	            		color : "white"
	            	}
	            }
	        },
	        plotOptions: {
	            line: {
	                dataLabels: {	
	                    enabled: false,
	                    color: 'white',
	                    style: {
	                        textShadow: false 
	                    }
	                },
	                enableMouseTracking: false
	            }
	        },
	        series: [{
	            name: 'Spindle Load',
	            color : 'yellow',
	            //data: spdLoadPoint
	            data : spdLoadPoint,
	            marker : {
                    enabled : false,
                },
	        }, {
	            name: 'Spindle Override',
	            color : 'red',
	            data: spdOverridePoint,
	            marker : {
                    enabled : false,
                },
	        }]
	    });	
		
		spdChart = $('#spdFeed').highcharts();
	};
	
	var spdChart;
	$(function(){
		getDvcList();
		setStartTime();
		$(".menu").click(goReport);
		//getMousePos();
		$("#menu_btn").click(function(){
			if(!panel){
				showPanel();
			}else{
				closePanel();
			};
			panel = !panel;
		});
		
		$("#dvcName1").change(function(){
			var dvcId = $("#dvcName1").val(); 
			getDvcStatus(dvcId, 1);
		});
		
		if(session=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		$("#up").click(upDate);
		$("#down").click(downDate);
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds())); 
		
		today = year + "-" + month + "-" + day;
		$("#sdate").val(today);

		targetDate = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":" + second;
		today = year + "-" + month + "-" + day;
		 
		$("#sdate").change(changeDateVal);
		
		$("#loader").css("opacity", 0);
		$("#mainTable").css("opacity", 1);
		
		$("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		});
		
		$("#title_right").click(function(){
			location.href = "${ctxPath}/chart/main3.do";
		});
		
		$("#title_left").click(function(){
			location.href = "${ctxPath}/chart/main.do";
		});
		
		setElement();
		statusChart("status1");
		popup("popupChart");
		
		pieChart("pie1");
		
		drawGaugeChart("opTime1", "cuttingTime1", "가동율", "절분율","%");
		//drawGaugeChart("spdLoad1", "feedOverride1", "Spindle Load", "Feed Override",""); 
		
		detailBar = $("#popupChart").highcharts();
		setInterval(time, 1000);
		
		detailBar = $("#popupChart").highcharts();
		detailOptions = detailBar.options;
		
		$("#panel_table td").addClass("unSelected_menu");
		$("#menu9").removeClass("unSelected_menu");
		$("#menu9").addClass("selected_menu");
		
		//getAllDvcId();

		
		getDvcId();
		
		
		var pie1 = $("#pie1").highcharts();
		
		for(var i=0; i<=4; i++){
			pie1.series[0].data[i].update(0);
		};
		
		$("html").click(function(){
			$("#chartDataBox").fadeOut();
			$("#popup").fadeOut();
		});
		
		$("#mainTable").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("#mainTable").width()/2
		});
	});
	
	var dvcIdx = 0;
	var dvcArray = [];
	function getDvcId(){
		var url = "${ctxPath}/chart/getDvcId.do";
		var param = "shopId=" + shopId + 
					"&jig=CNC";
		
		$("#dvcName1").val(dvcArray[dvcIdx]);
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				dvcArray = [];
				alarmDvcArray = [];
				$(json).each(function(idx, data){
					dvcArray.push(data.dvcId);	
				});
				
				
				getDvcStatus(dvcArray[dvcIdx]);
				dvcIdx++;
				if(dvcIdx>=dvcArray.length) dvcIdx=0;
				
				setTimeout(getDvcId, 5000)
				
				//getData(dvcArray[0]);
			}
		});
	};
	
	function getSummaryDate(dvc){
		var dvcId = dvc[0];
		console.log(dvcId)
	};
	
	function parsingModal(obj, idx){
		var json = $.parseJSON(obj);
	
		var auxCode = json.AUX_CODE;
		var gModal = json.G_MODAL;
		
		var auxTR = "<tr>";
		var auxTD;
		var gModalTR = "<tr>";
		var gModalTD;
		
		$.each(auxCode, function(key, data){
			$.each(auxCode[key], function (key, data){
				auxTD += "<td>" + key + " : " + data + "</td>"; 
			});
		}); 
		
		auxTR += auxTD + "</tr>";
		
		$.each(gModal, function(key, data){
			$.each(gModal[key], function (key, data){
				gModalTD += "<td>" + key + " : " + data + "</td>";
			});
		}); 
		
		gModalTR += gModalTD + "</tr>";
		
		$("#modalTbl" + idx).append(auxTR);
		$("#modalTbl" + idx).append(gModalTR);
	};

	function popup(id){
		$('#' + id).highcharts({
			chart : {
				
				type : 'bar',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: 200
			},
			credits : false,
			//exporting: false,
			title : false,
			xAxis : {
				categories : [""],
				labels : {
					style : {
						fontSize : '35px',
						fontWeight:"bold",
						color : "white"
					}
				}
			},
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			yAxis : {
				min : 0,
				max : 59,
				tickInterval:1,
				reversedStacks: false,
				title : {
					text : false
				},
				labels: {
	                formatter: function () {
	                	var value = labelsArray_hour[this.value]
	                    return value;
	                },
	                style :{
	                	color : "black",
	                	fontSize : "20px"
	                },
	            },
			},
			legend : {
				enabled : false
			},
			plotOptions : {
				series : {
					  dataGrouping : {
		                    forced : true,
		                    units : [['minute', [5]]]
		                },
					stacking : 'normal',
					pointWidth:120, 
					borderWidth: 0,
					animation: false,
					cursor : 'pointer',
					point : {
						events: {
							click: function (e) {
		                   	}
		              	}
					}
				},
			},
			series : [{
				data :[0.16],
				color : "Red"
			},
			{
				data :[0.16],
				color : "green"
			},
			{
				data :[0.16],
				color : "yellow"
			},
			{
				data :[0.16],
				color : "Red"
			},
			{
				data :[0.16],
				color : "green"
			},
			{
				data :[0.16],
				color : "Red"
			}]
		});
	};
	
	var labelsArray_hour = [0,1,2,3,4,5,6,7,8,9,
	                        10,11,12,13,14,15,16,17,18,19,
	                        20,21,22,23,24,25,26,27,28,29,
	                        30,31,32,33,34,35,36,37,38,39,
	                        40,41,42,43,44,45,46,47,48,49,
	                        50,51,52,53,54,55,56,57,58,59
	                        ];
	
	var dvcArray = new Array();
	var dvcIndex = 0;
	/* function getAllDvcId(){
		var url = "${ctxPath}/chart/getAdapterId.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dvcId;
				$(json).each(function(i, obj){
					var dvc = new Array();
					dvc.push(obj.dvcId, obj.name);
					dvcArray.push(dvc);
				});
				
				getDvcStatus(dvcArray[dvcIndex], 1);
				getDvcStatus(dvcArray[dvcIndex+1], 2);
				
				setInterval(resetArray,1000 * 20);
				
				currentStatusLoop1= setInterval(function(){getCurrentDvcStatus(dvcArray[dvcIndex][0], 1)}, 3000)
				currentStatusLoop2= setInterval(function(){getCurrentDvcStatus(dvcArray[dvcIndex+1][0], 2)}, 3000)
			}
		});
	}; */
	
	function resetArray(){
		dvcMap1.put("flag", true);
		dvcMap1.put("initFlag", true);
		dvcMap1.put("currentFlag", true);
		dvcMap1.put("noSeries", false);
		clearInterval(currentStatusLoop1);
		clearInterval(statusLoop1);
		dvcMap2.put("flag", true);
		dvcMap2.put("initFlag", true);
		dvcMap2.put("currentFlag", true);
		dvcMap2.put("noSeries", false);
		clearInterval(currentStatusLoop2);
		clearInterval(statusLoop1);
		
		dvcIndex+=2;
		if(dvcIndex>=dvcArray.length)dvcIndex=0;
		
		getDvcStatus(dvcArray[dvcIndex], 1);
		getDvcStatus(dvcArray[dvcIndex+1], 2);
	};
	var statusLoop1, statusLoop2;
	var currentStatusLoop1, currentStatusLoop2;
	
	function isItemInArray(array, item) {
		for (var i = 0; i < array.length; i++) {
			for (var j = 0; j < array[i].length; j++) {
				if (array[i][j] == item) {
					return true;   // Found it
		        };
	        };
	    };
		return false;   // Not found
	};
	
	var spdLoadPoint = [];
	var spdOverridePoint = [];
	
	var block = 1/6;
	function getDvcStatus(dvc, idx){
		var dvcId = dvc;
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		//var today = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":" + second;
		
		
		if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
			day = addZero(String(new Date().getDate()+1));
		};
		
		var today = year + "-" + month + "-" + day;
		
		var url = "${ctxPath}/chart/getTimeData.do";
		var param = "dvcId=" + dvcId + 
					"&workDate=" + today;
		
		setInterval(function (){
			var minute = String(new Date().getMinutes());
			if(minute.length!=1){
				minute = minute.substr(1,2);
			};
			
			if(minute==5 && eval("dvcMap" + idx).get("initFlag")){
				console.log(minute,eval("dvcMap" + idx).get("initFlag"))
				console.log("init")
				var today = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":" + second;
				targetDate = today;
				
				getDvcStatus(dvc, idx);
				eval("dvcMap" + idx).put("initFlag", false);
				eval("dvcMap" + idx).put("currentFlag", true);
			}else if(minute!=5){
				eval("dvcMap" + idx).put("initFlag", true);
			};
		}, 1000 * 10);
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				$("#loader").css("opacity", 0);
				$("#mainTable").css("opacity", 1);
	
				var status = $("#status1").highcharts();
				var options = status.options;
				
				options.series = [];
				options.title = null;
				
				var json = data.statusList;
				
				var color = "";
				
				var status = json[0].status;
				
				if(status=="IN-CYCLE"){
					color = "green"
				}else if(status=="WAIT"){
					color = "yellow";
				}else if(status=="ALARM"){
					color = "red";
				}else if(status=="NO-CONNECTION"){
					color = "gray";
				};
				
				var blank;
				var f_Hour = json[0].startDateTime.substr(11,2);
				var f_Minute = json[0].startDateTime.substr(14,2);
				
				var startHour = 20;
				var startMinute = 3;
				
				var startN = 0;
				if(f_Hour==startHour && f_Minute==(startMinute*10)){
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : color
						} ],
					});
				}else{
					if(f_Hour>=20){
						startN = (((f_Hour*60) + (f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
					}else{
						startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
						startN += (f_Hour*60/2) + (f_Minute/2);
					};
					
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : "gray"
						} ],
					});
						
					for(var i = 0; i < startN-1; i++){
						options.series[0].data.push({
							y : Number(20),
							segmentColor : "gray"
						});
					};
				};
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				$(json).each(function(idx, data){
					spdLoadPoint.push(Number(data.spdLoad));
					spdOverridePoint.push(Number(data.spdOverride));
					
					if(data.status=="IN-CYCLE"){
						color = "green"
					}else if(data.status=="WAIT"){
						color = "yellow";
					}else if(data.status=="ALARM"){
						color = "red";
					}else if(data.status=="NO-CONNECTION"){
						color = "gray";
					};
					options.series[0].data.push({
						y : Number(20),
						segmentColor : color
					});
				});
				
				
				for(var i = 0; i < 719-(json.length+startN); i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "rgba(0,0,0,0)"
					});
					
					spdLoadPoint.push(Number(-10));
					spdOverridePoint.push(Number(-10));
				};
				
				status = new Highcharts.Chart(options);
				
				getCurrentDvcStatus(dvcId, 1); 
				getSpdFeed();
			}
		});
	};

	var labelsArray = [20,21,22,23,24,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	
	function drawPieData(chartStatus, inCycleTime, waitTime, alarmTime, offTime, chart){
		chart.series[0].data[0].update(inCycleTime);		
		chart.series[0].data[1].update(waitTime);
		chart.series[0].data[2].update(alarmTime);
		chart.series[0].data[3].update(offTime);
		
		var day = 24*60*60;
		var blank = day-(inCycleTime + waitTime + alarmTime + offTime);
		chart.series[0].data[4].update(blank);
	};
	
	function calcTime(startTime, endTime){
		var startH = Number(startTime.substr(0,2));
		var startM = Number(startTime.substr(3,2));
		var startS = Number(startTime.substr(6,2));

		var endH = Number(endTime.substr(0,2));
		var endM = Number(endTime.substr(3,2)); 
		var endS = Number(endTime.substr(6,2));
		
		return ((endH*60*60) + (endM*60) + endS) - ((startH*60*60) + (startM*60) + startS);  
	};
	
	function removeSpace(str){
		return str = str.replace(/ /gi, "");
	};
	
	var dvcMap1 = new JqMap();
	var dvcMap2 = new JqMap();
	
	dvcMap1.put("flag", true);
	dvcMap1.put("initFlag", true);
	dvcMap1.put("currentFlag", true);
	dvcMap1.put("noSeries", false);
	
	dvcMap2.put("flag", true);
	dvcMap2.put("initFlag", true);
	dvcMap2.put("currentFlag", true);
	dvcMap2.put("noSeries", false);
	
	function getAlarmList(dvcId){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		var url = "${ctxPath}/chart/getAlarmList.do";
		var sDate = today;
		var eDate = today + " 23:59";
		//var workDate = $("#sdate").val();
		var param = "dvcId=" + dvcId +
					"&sDate=" + sDate + 
					"&eDate=" + eDate;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				/* var json = data.alarmList;
				
				$(json).each(function (i,data){
					$("#alarmCode1_" + i).html("[" + data.startDateTime.substr(5,14)+"] " + data.alarmCode + " - ");
					$("#alarmMsg1_" + i).html(data.alarmMsg);
				}); */
			}
		});
		
	};
	
	function getCurrentDvcStatus(dvcId, idx){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId + 
					//"&workDate=" + $("#sdate").val();
				"&workDate=" + today;
				
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var type = data.type;
				var progName = data.lastProgramName;
				var progHeader = data.lastProgramHeader;
				var name = data.name;
				var inCycleTime = data.inCycleTime;
				var waitTime = data.waitTime;
				var alarmTime = data.alarmTime;
				var noConTime = data.noConnectionTime;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var opRatio = data.opRatio;
				var cuttingRatio = data.cuttingRatio;
				var alarm = data.alarm;
				
				$("#alarmCode" + idx +"_0").html("");
				$("#alarmMsg" + idx +"_0").html("");
				$("#alarmCode" + idx +"_1").html("");
				$("#alarmMsg" + idx +"_1").html("");
				
				//if(alarm!=null && alarm!="" && typeof(alarm)!="undefined") parsingAlarm(idx, alarm);
				//getAlarmList(dvcId);
				
				var lamp = "<img src=${ctxPath}/images/DashBoard/" + chartStatus +".png style='height:" + getElSize(50) +"px;vertical-align: text-top;'>";
				
				if(data.chartStatus=="null"){
					lamp = "<img src=${ctxPath}/images/DashBoard/NO-CONNECTION.png style='height:" + getElSize(50) +"px;vertical-align: text-top;'>";
					name = window.sessionStorage.getItem("name");
				}
				$("#progName" + idx).html(progName);
				$("#progHeader" + idx).html(progHeader);
				//$("#dvcName" + idx).html(name + lamp);
				//$("#dvcName1").val(dvcId);
				
				if(String(type).indexOf("IO")!=-1){
					$("#progHeader" + idx).append("I/O Logik");
				};
				
				
				if(today==$("#sdate").val()){
					var status = $("#status" + idx).highcharts();
					var options = status.options;
					
					if(eval("dvcMap" + idx).get("noSeries")){
						options.series = [];
						options.title = null;
						
		      			options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : slctChartColor(chartStatus) 
					        	}],
					    });	
					}else{
						options.title = null;
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : slctChartColor(chartStatus)
			  			});		
					};
					
		      	  	var now = options.series[0].data.length;
					var blank = 144 - now;
					
					for(var i = 0; i <= blank; i++){
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : "rgba(0,0,0,0.0)"
			  			});	
					};
					
					status = new Highcharts.Chart(options);
					
					eval("dvcMap" + idx).put("currentFlag", false)
		      	};
		      	
				var pieChart = $("#pie" + idx).highcharts();
				drawPieData(chartStatus, inCycleTime, waitTime, alarmTime, noConTime, pieChart);
				
				var opTimeChart = $("#opTime" + idx).highcharts().series[0].points[0];
				opTimeChart.update(Number(Number(opRatio).toFixed(1)));
			
				var cuttingTime = $("#cuttingTime" + idx).highcharts().series[0].points[0];
				cuttingTime.update(Number(Number(cuttingRatio).toFixed(1)));
				
				//var spd = $("#spdLoad" + idx).highcharts().series[0].points[0];
				//spd.update(Number(spdLoad));
				
				//var feed = $("#feedOverride" + idx).highcharts().series[0].points[0];
				//feed.update(Number(feedOverride));
				
				var statusLoop;
				if(idx==1){
					statusLoop = currentStatusLoop1;
				}else{
					statusLoop = currentStatusLoop1;
				};
				
				setInterval(function(){
					getCurrentDvcStatus(dvcId, 1)					
				}, 1000*60*10)
			}
		});
		
	};
	
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function removeHypen(str){
		str = str.replace(/-/gi,"");
		return str;
	};
	
	var block = 1/6;

	function parsingAlarm(idx,alarm){
		var json = $.parseJSON(alarm);
		
		$(json).each(function (i,data){
			$("#alarmCode" + idx + "_" + i).html(data.alarmCode + " - ");
			$("#alarmMsg" + idx + "_" + i).html(data.alarmMsg);
		});
	};
	
	function drawGaugeChart(el1, el2, title1, title2, unit){
		var gaugeOptions = {
				chart: {
					type: 'solidgauge',
			     	backgroundColor : 'rgba(255, 255, 255, 0)',
			  	},
			  	title : false,
				credits : false,
			   	exporting : false,
			 	pane: {
			 		center: ['50%', '80%'],
			      	size: '100%',
	         		startAngle: -90,
	         		endAngle: 90,
	         		background: {
	             	backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
	             	innerRadius: '60%',
	             	outerRadius: '100%',
	             	shape: 'arc'
	         		}
			 	},
        		tooltip: {
        			enabled: false
			  	},
			 	yAxis: {
			 		stops: [
			 		        [1, '#13E000']
			            	],
			            	lineWidth: 0,
			            	minorTickInterval: null,
			            	tickPixelInterval: 400,
			            	tickWidth: 0,
			            	title: {
			                y: -70
			            	},
			            labels: {
			                y: 16
			            	}
			 	},
			 	plotOptions: {
			            solidgauge: {
			                dataLabels: {
			                    y: 5,
			                    borderWidth: 0,
			                    useHTML: true
			                }
			            }
			        }
			    };

				var max;
				if(unit=="%"){
					max = 100
				}else{
					max = 200;
				};
			    // The speed gauge
			    $('#' + el1).highcharts(Highcharts.merge(gaugeOptions, {
			    	  yAxis: {
			    		  labels : {
								enabled : false,
							},
				            min: 0,
				            max: max,
				            title: {
				                text: title1,
				               y: -getElSize(160),
				               style:{
				            	   color : "black",
				            	   fontSize : getElSize(40) + 'px',
									fontWeight:"bold"
				            	   
				               }
				    		},
				        },

			        credits: {
			            enabled: false
			        },

			        series: [{
			            name: '가동률',
			            data: [0],
			            dataLabels: {
			                format: '<div style="text-align:center"><span style="font-size:'+ getElSize(50 )+ 'px;color:white">{y}</span>' +
			                       '<span style="font-size:' + getElSize(30) + ';color:white">' + unit + '</span></div>'
			            },
			            tooltip: {
			                valueSuffix: ' %'
			            }
			        }]

			    }));

			    // The RPM gauge
			    $('#' + el2).highcharts(Highcharts.merge(gaugeOptions, {
			    	 yAxis: {
			    		 labels : {
								enabled : false,
							},
				            min: 0,
				            max: max,
				            title: {
				                text: title2,
				               y: -getElSize(160),
				               style:{
				            	   color : "black",
				            	   fontSize : getElSize(40) + 'px',
									fontWeight:"bold"
				               }
									
				            },
				        },

			        series: [{
			            name: '절분률',
			            data: [0],
			            dataLabels: {
			                format: '<div style="text-align:center"><span style="font-size:' + getElSize(50) + 'px;color:white">{y}</span>' +
			                       '<span style="font-size:' + getElSize(30) + 'px;color:white">' + unit + '</span></div>'
			            },
			            tooltip: {
			                valueSuffix: '%'
			            }
			        }]

			    }));
	};
	
	function pieChart(id){
		Highcharts.setOptions({
			//green yellow red black
			   colors: ['#148F01', '#C7C402', '#ff0000', '#8C9089', '#ffffff']
		    });
		
	    
	    // Radialize the colors
	    Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
	        return {
	            radialGradient: { cx: 0.5, cy: 0.5, r: 0.7 },
	            stops: [
	                [0, Highcharts.Color(color).brighten(0.5).get('rgb')], // darken
	                [1, color]
	                
	            ]
	        };
	    });
	    
		$('#' + id)
		.highcharts(
				{
					chart : {
						plotBackgroundColor : null,
						plotBorderWidth : null,
						plotShadow : false,
						backgroundColor : 'rgba(255, 255, 255, 0)',
						type: 'pie',
						options3d: {
			                enabled: true,
			                alpha: 45
			            } 
					},
					credits : false,
					title : {
						text : false
					},
					tooltip : {
						pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>',
						enabled : false
					},
					plotOptions : {
						pie : {
							innerSize: getElSize(130),
				      		depth: contentHeight/(targetHeight/45),
							size:'95%',
							allowPointSelect : true,
							cursor : 'pointer',
							dataLabels : {	 
								enabled : true,
							 	formatter : function(){
							 		var value;
							 		if(this.y!=0.0) value = Number(this.y/60/60).toFixed(1);
							 		return value;
							 	},
							 	connectorColor: '#000000',
								distance : 1,
							 	style : {
							 		 color: 'black',
							 		 textShadow: '0px 1px 2px black',
							 		 fontSize : getElSize(23),
								}
							}
						}
					},
					exporting: false,
					series : [ {
						type : 'pie',
						name : 'Chart Status',
						data : [ [ 'In-Cycle',1], 
						         [ 'Wait', 1 ],
						         [ 'Alarm', 1 ],
						         [ 'Power-Off', 1],
						         [ 'blank', 1],
								]
					} ]
				});
	};
	
	var detailOptions;
	var detailBarArray = new Array();
	function addDetailBar(bar, color){
		var _bar = new Array();
		_bar.push(bar);
		_bar.push(color);
		
		detailBarArray.push(_bar);
	};
	
	function reDrawDetailBar(){
		detailOptions.series = [];
		detailOptions.title = null;
		for (var i = 0; i < detailBarArray.length; i++){
			detailOptions.series.push({
		        data: [{
		        		y : detailBarArray[i][0],
		        	}],
		        color : detailBarArray[i][1]
		    });	
		};
		
		detailBar = new Highcharts.Chart(detailOptions);
	};
	
	var barSize = 1/6;
	var detailBarMap = new JqMap();
	
	function getDetailStatus(dvcId, dvcName, hour){
		if(detailBarArray.length!=0){
			detailBarArray = new Array();
			for(var i = 0; i < detailBarArray.length; i++){
				detailBar.series[0].remove(true);
			};			
		};
		
		if(hour==24) hour=0;
		
		var url = "${ctxPath}/chart/getDetailStatus.do";
		var data = "dvcId=" + dvcId + 
					"&startDateTime=" + hour;
		
		$.ajax({
			url : url,
			data : data,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.chartStatus;
				
				$(json).each(function(i, data){
					var startTime = removeHypen(data.startDateTime, "690").substr(9,8);
					var endTime = removeHypen(data.endDateTime, "691").substr(9,8);
					var color = slctChartColor(removeSpace(data.chartStatus));
					
					var startH = Number(startTime.substr(0,2));
					var startM = String(startTime.substr(3,2));
					var startS = Number(startTime.substr(6,2));

					var endH = Number(endTime.substr(0,2));
					var endM = String(endTime.substr(3,2)); 
					var endS = Number(endTime.substr(6,2));

					var start = (startH*60*60) + (startM*60) + (startS);
					var end = (endH*60*60) + (endM*60) + (endS);
					
					if(i==0)start=hour*60*60;
					var bar = (end - start)/10*barSize;
					
					if(bar<0.16)bar=0.16;
					detailBarMap.put("chart", detailBar);
					
					addDetailBar(bar, color)
				});
				
				reDrawDetailBar();
				
				$("#popup").fadeToggle();
				$("#hour").html(dvcName + " (" + hour + ":00 ~ " + hour + ":59)");	
			}
		});
	};
	
	function setStartTime(){
		var url = "${ctxPath}/chart/getStartTime.do"
		
		$.ajax({
			url :url,
			dataType :"text",
			type : "post",
			success : function(data){
				startTime = Number(data);
				workStartTime = startTime-1;
				for(var i = 0; i<=24; i++){
					if(i+startTime>24){
						startTime -= 24;
					};
					
					//timeLabel.push(i+startTime);
					for(var j = 0; j < 5; j++){
						//timeLabel.push(0);	
					};
				};					
			}
		});
	};
	
	//var timeLabel = [];
	
	var startTimeLabel = new Array();
	
	var startHour = 20;
	var startMinute = 3;
	
	var colors;
	function statusChart(id){
		var m0 = "",
		m02 = "",
		m04 = "",
		m06 = "",
		m08 = "",
		
		m1 = "";
		m12 = "",
		m14 = "",
		m16 = "",
		m18 = "",
		
		m2 = "";
		m22 = "",
		m24 = "",
		m26 = "",
		m28 = "",
		
		m3 = "";
		m32 = "",
		m34 = "",
		m36 = "",
		m38 = "",
		
		m4 = "";
		m42 = "",
		m44 = "",
		m46 = "",
		m48 = "",
		
		m5 = "";
		m52 = "",
		m54 = "",
		m56 = "",
		m58 = "";
	
	var n = Number(startHour);
	if(startMinute!=0) n+=1;
	
	for(var i = 0, j = n ; i < 24; i++, j++){
		eval("m" + startMinute + "=" + j);
		
		startTimeLabel.push(m0);
		startTimeLabel.push(m02);
		startTimeLabel.push(m04);
		startTimeLabel.push(m06);
		startTimeLabel.push(m08);
		
		startTimeLabel.push(m1);
		startTimeLabel.push(m12);
		startTimeLabel.push(m14);
		startTimeLabel.push(m16);
		startTimeLabel.push(m18);
		
		startTimeLabel.push(m2);
		startTimeLabel.push(m22);
		startTimeLabel.push(m24);
		startTimeLabel.push(m26);
		startTimeLabel.push(m28);
		
		startTimeLabel.push(m3);
		startTimeLabel.push(m32);
		startTimeLabel.push(m34);
		startTimeLabel.push(m36);
		startTimeLabel.push(m38);
		
		startTimeLabel.push(m4);
		startTimeLabel.push(m42);
		startTimeLabel.push(m44);
		startTimeLabel.push(m46);
		startTimeLabel.push(m48);
		
		startTimeLabel.push(m5);
		startTimeLabel.push(m52);
		startTimeLabel.push(m54);
		startTimeLabel.push(m56);
		startTimeLabel.push(m58);
		
		if(j==24){ j = 0}
	};
	
	
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
	//	$('#' + id).highcharts({
		options = {
			chart : {
				
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: getElSize(300),
				marginTop: -100
			},
			credits : false,
			exporting: false,
			title : false,
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			xAxis:{
		           categories:startTimeLabel,
		            labels:{
		               
		            	step: 1,
						formatter : function() {
							var val = this.value

							return val;
						},
			                        style :{
			    	                	color : "white",
			    	                	fontSize : getElSize(15)
			    	                },
		            }
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    }
			},
			legend : {
				enabled : false
			},
				series: []
		}
		
		//});
	        
	      /*   Highcharts.setOptions({
    	        yAxis:{
    	        	labels:{
    	        		formatter: function () {
    	                	var value = labelsArray[this.value];
    	                    return value;
    	                }
    	         	}   
    	      	}
    	    }); */

	   	$("#" +id).highcharts(options);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#sdate").css({
			"position" : "absolute",
			"right" :getElSize(30),
			"top" : 0
		});
		
		$("#date_controller button").css({
			"font-size" : getElSize(40),
			"width" : getElSize(100),
			"height" : getElSize(60)
		});
		
		$("#date_controller").css({
			"position" : "absolute",
			"top" : 0,
		});

		$("#date_controller").css({
			"position" : "absolute",
			"right" : $("#sdate").width() + ($("#date_controller").width()/2),
			"top" : 0,
		});
		
		$("#popup").css({
			"top" : $("#status1").offset().top,
			"left" : width/2 - ($("#popup").width()/2),
			"z-index" : 99999
		});
		
		$("#title_main").css({
			"left" : width/2 - $("#title_main").width()/2
		});
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		
		$("#loader").css({
			"left" : width/2 - $("#loader").width()/2,
			"top" : height/2 - $("#loader").height()/2
		});
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2),
		});
		
		$("#title_main").css({
			"width" : getElSize(1000),
			"top" : $("#container").offset().top + (getElSize(50)),
		});
		
		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"left" : $("#container").offset().left + getElSize(50),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_right").css({
			"width" : getElSize(300),
			"right" : $("#container").offset().left,
			"color" : "white",
			"font-weight" : "bolder",
			"font-size" : getElSize(40),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(120),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(120),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$(".status").css({
			"width" : contentWidth * 0.9
		});
		
		$("hr").css({
			"width" : contentWidth * 0.9,
			"top" : $("#container").offset().top + getElSize(250),
		});
		
		$("hr").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("hr").width()/2,
		});
		
		$("#mainTable").css({
			"width" : contentWidth * 0.9,
			"top" : $("#container").offset().top + getElSize(300)
		});
		
		
		$(".font_dvc_name").css({
			"font-size" : getElSize(50) + "px",
			"margin-left" : getElSize(10) + "px"
		});
		
		$(".mode").css({
			"font-size": getElSize(40) + "px",
			"margin-right": getElSize(20) + "px",
		});
		
		$(".tableTitle").css({
			"font-size" : getElSize(40) + "px"
		});
		
		$(".title_td").css({
			"padding" : getElSize(20) + "px"
		});
		
		$(".pie_cell").css({
			"height" : getElSize(800)
		});
		
		$("#gaugeDiv1").css({
			"width": getElSize(1500),
			"height": getElSize(400),
			"left": getElSize(1900),
			"top": getElSize(930)
		});
		
		$("#gaugeDiv2").css({
			"width": getElSize(1500),
			"height": getElSize(400),
			"left": getElSize(1900),
			"top": getElSize(980)
		});
		
		$("#gaugeDiv3").css({
			"width": getElSize(700),
			"height": getElSize(400),
			"left": getElSize(2640),
			"top": getElSize(680)
		});
		
		$("#gaugeDiv4").css({
			"width": getElSize(700),
			"height": getElSize(400),
			"left": getElSize(2640),
			"top": getElSize(980)
		});
		
		$(".mainTd").css({
			"padding" : "0 " + getElSize(50) + " 0 " + getElSize(50)
		});
		
		$(".pie").css({
			"height" : getElSize(800)
		});
		
		$(".alarmDiv").css({
			"margin-top": getElSize(300),
			"margin-left":getElSize(50),
			"margin-right":getElSize(50),
			"line-height": getElSize(50) + "px"
		});
		
		$(".alarmText").css({
			"font-size" : getElSize(50)
		});
		
		$(".gauge").css({
			"height" : getElSize(600)
		});
		
		$("#sdate").css({
			"font-size" : getElSize(30)
		});
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"left" : 0,
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(60)
		});

		$("#panel_table td").css({
			"padding" : getElSize(50),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$('#spdFeed').css({
			"margin-left" : -getElSize(100)
		})
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
</script>
<style>

@font-face {font-family:MalgunGothic; src:url(../fonts/malgun.ttf);}

*{
	font-family:'MalgunGothic';
}

#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	background-color: black;
  	font-family:'Helvetica';
	background-size : 100% 100%;
	overflow: hidden;
}

.title{
	top : 50px;
	position: absolute;
}

#title_main{
	width: 1000px;
	z-index: 999;
}
#title_left{
	left : 50px;
	width: 300px;
}

#title_right{
	right: 50px;
	width: 300px;
}

#pieChart1{
	position: absolute;
	left : 0px;
	z-index: 99;
	top: 800px;
	height: 500px;
}

#pieChart2{
	position: absolute;
	right : 0px;
	z-index: 99;
	top: 800px;
	height: 500px;
} 

#time{
	position: absolute;
	top: 170px;
	right: 50px;	
	font-size : 30px;
	color: white;
} 
#date{
	position: absolute;
	top: 170px;
	right: 250px;	
	font-size : 30px;
	color: white;
} 

hr{
	position: absolute;
	top: 250px;
	z-index: 99;
	border: 2px solid white;
}

#mainTable{
	position: absolute;
	z-index: 99;
	opacity : 0;
}

#mainTable tr:last-child td:first-child {
    -moz-border-radius-bottomleft:10px;
    -webkit-border-bottom-left-radius:10px;
    border-bottom-left-radius:10px;
    
    -moz-border-radius-topleft:10px;
    -webkit-border-top-left-radius:10px;
    border-top-left-radius:10px
}

#mainTable tr:last-child td:last-child {
    -moz-border-radius-bottomright:10px;
    -webkit-border-bottom-right-radius:10px;
    border-bottom-right-radius:10px;
    
    -moz-border-radius-topright:10px;
    -webkit-border-top-right-radius:10px;
    border-top-right-radius:10px
}

.statusIcon{
	width: 80px;
	height: 80px;
}

.pie{
	width: 95%;
	margin: 0 auto;
}

.tr{
  background: linear-gradient(white, gray);
  opacity : 0.7;
  
}
#mainTable{
	opacity : 0;
}
#loader{
	position: absolute;
	width: 500px;
	-webkit-filter: brightness(10);
	/* background-color: white;
	-webkit-border-radius: 50em;
	-moz-border-radius: 50em;
	border-radius: 50em; */
}
#popup{
	width: 100%;
}

.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style>
<script type="text/javascript">
	
</script>

</head>


<body>
	<div id="container" >
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">샵 레이아웃</td>
				</tr>
				<tr>
					<td id="menu9" class="menu">개별 장비 가동 현황</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">장비별 가동실적 분석</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
				</tr>
				<tr>
					<td id="menu3" class="menu">무인장비 가동 현황</td>
				</tr>
				<tr>
					<td id="menu4" class="menu">설비현황</td>
				</tr>
				<!-- <tr>
					<td id="menu4" class="menu">야간무인장비 가동 현황</td>
				</tr> -->
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
		<div id="svg"></div>
	</div>
	<div id="popup" style="background-color:white; position: absolute; display: none;border-radius : 20px;">
		<center><div id="hour" style="font-size: 60px; font-weight: bold;"></div></center>
		<div id='popupChart' style="width: 100%" ></div>
	</div>
	
	<img src="${ctxPath }/images/DashBoard/title2.svg" id="title_main" class="title">
	<%-- <img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title"> --%>
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title"> --%>	
	<div id="title_right" class="title">(주) 부광정밀</div>	
	
	<hr>

	<font id="date"></font>
	<font id="time"></font>
	
	<img alt="" src="${ctxPath }/images/DashBoard/loader.png" id="loader">
	<table id="mainTable" style="margin-bottom: 0px">
		<tr>
			<td style="border-right: 2px solid white; width: 50%; padding: 0 50 0 50" class="mainTd">
				<!-- <font style="font-weight: bold; color: white; font-size: 50px;" id="dvcName1" class="font_dvc_name"></font><br> -->
				<select id="dvcName1">
				
				</select>
				<span style="font-weight: bold; color: white; font-size: 50px" id="progName1" class="font_dvc_name"></span> 
				<span style="font-weight: bold; color: white; font-size: 50px" id="progHeader1" class="font_dvc_name"></span>

				
				<!-- <div id="date_controller">
					<button id="up">⬆︎</button><button id="down">⬇︎</button>
				</div>
				
				<input type="date" id="sdate" data-date-format="DD MMMM YYYY" > -->
				<div id="status1" class="status"></div>

				<div id="spdFeed"></div>				
				<div style="float: right; font-weight: bold; font-size: 40px; color: white; margin-right: 20px" class="mode">Mode : Automatic</div>
				<table class="subTable" style="width:100%"> 
					<tr style="color: black; font-size: 40px; background-color: #fff8dc; font-weight: bold;" class="tableTitle">
						<td width="50%" align="center" style="padding: 20px;" class="title_td">
							일 누적 가동현황
						</td>
						<td width="50%" align="center">
							현 가동 상태
						</td>
					</tr>
					<tr class="tr">
						<td width="50%"  class="pie_cell">
							<div class="pie" id="pie1"></div>
						</td>
						<td >
							<div style="position: absolute;"  id="gaugeDiv1">
								<div id="opTime1" style="float: left; width: 40%; " class="gauge"> </div>
								<div id="cuttingTime1" style="float: right; width: 40%" class="gauge"></div>
							</div>
							<!-- <div style="width: 700px; position: absolute; left: 920; top: 980px" id="gaugeDiv2">
								<div id="spdLoad1" style="float: left; width: 40%" class="gauge"></div>
								<div id="feedOverride1" style="float: right; width: 40%" class="gauge"></div>
							</div> -->
							<div style="margin-top:0px;margin-left:50px;margin-right:50px;;  text-align: left; line-height: 50px" class="alarmDiv">
								<font style="font-size: 30px; font-weight: bold; color: red;"  class="alarmText">Alarm</font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode1_0" class="alarmText"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg1_0" class="alarmText"></font><br>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmCode1_1" class="alarmText"></font>
								<font style="font-size: 30px; font-weight: bold; color: black;" id="alarmMsg1_1" class="alarmText"></font><br>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>