var draw;
var background_img;
var marker;
var imgPath = "../images/DashBoard/";

var pieChart1;
var pieChart2;
var borderColor = "";
var totalOperationRatio = 0;

$(function() {
	//pieChart1 = $("#pieChart1").highcharts();
	pieChart2 = $("#pieChart2").highcharts();
	
	$("#title_main").css({
		left : window.innerWidth/2 - ($("#title_main").width()/2)
	});
	
	draw = SVG("svg");
	
	getMachineInfo();
	getMarker();
	
	background_img = draw.image(imgPath+"Road.png").size(window.innerWidth * 0.9, window.innerHeight);
	background_img.x(150);
	background_img.y(100);
	
//	var logoText =  draw.text(function(add) {
//		  add.tspan("　　　성역 없는 ").fill("#313131");
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("전원이 참여하는 ").fill("#0005D5").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("　　나로 부터의 ").fill("#c00000").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  
//	});
//		
//	logoText.font({
//			'font-family':'나눔고딕'
//			, size:     75
//			, anchor:   'right'
//			, leading:  '1.3em'
//		   ,'font-weight':'bolder',
//	});
//	
//	logoText.x(2600);
//	logoText.y(1730);
});

function alarmTimer(){
	setInterval(function(){
		if(s2==10){
    		s++;
    		$("#second1").html(s);
    		s2 = 0;	
    	};
    	
    	if(s==6){
    		s = 0;
    		m2++;
    		
    		$("#second1").html(s);
    		$("#minute2").html(m2);
    	};
    	
    	if(m2==10){
    		m2 = 0;
    		m++;
    		$("#minute1").html(m);
    	};
		
    	$("#second2").html(s2);
    	s2++;
    	
	},1000);
};

function redrawPieChart(){
	var url = ctxPath + "/svg/getMachineInfo2.do";
	
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;

			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			$(json).each(function(key, data){
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				}
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "blue";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			bodyNeonEffect(borderColor);
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
		}
	});
};

function printMachineName(arrayIdx, x, y, w, h, name, color){
	machineName[arrayIdx] = draw.text(nl2br(name)).fill(color);
	machineName[arrayIdx].font({
		  family:   'Helvetica'
		, size:     25
		, anchor:   'middle'
		, leading:  '1.5em'
	});
	
	machineName[arrayIdx].x(x + (w/2));
	machineName[arrayIdx].y(y + (h/2)-20);
	
	machineName[arrayIdx].leading(1);
};

function nl2br(value) {
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;
function getMachineInfo(){
	var url = ctxPath + "/svg/getMachineInfo.do";
	
	
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;
			newMachineStatus = new Array();
			
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;

			operationTime = 0;
			$(json).each(function(key, data){
				var array = new Array();
				var machineColor = new Array();
				
				//console.log(data.lastChartStatus, key)
				array.push(data.id);
				array.push(data.name);
				array.push(data.x);
				array.push(data.y);
				array.push(data.w);
				array.push(data.h);
				array.push(data.pic);
				array.push(data.lastChartStatus);
				
				operationTime += Number(data.operationTime);
				
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				};
				
				machineProp.push(array);
				/*machineProp.push("machine");
				machineProp.push("name");*/
				
				machineColor.push(data.id);
				machineColor.push(data.lastChartStatus);
				
				newMachineStatus.push(machineColor);
			});
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
			
			if(!compare(machineStatus,newMachineStatus)){
				reDrawMachine();
				redrawPieChart();
			};
			
			if(first){
				for(var i = 0; i < machineProp.length; i++){
					//array = id, name, x, y, w, h, pic, status
					drawMachine(machineProp[i][0],
							machineProp[i][1],
							machineProp[i][2],
							machineProp[i][3],
							machineProp[i][4],
							machineProp[i][5],
							machineProp[i][6],
							machineProp[i][7],
							i);
				};
				first = false;
			};
			
			//cal totalOperationRatio
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
			
			var totalMachine = 0;
			totalMachine += (inCycleMachine + waitMachine + alarmMachine);
			var totalOperationRatio = Number(inCycleMachine / totalMachine * 100).toFixed(1);
			pieChart1.series[0].data[0].update(Number(totalOperationRatio));
			pieChart1.series[0].data[1].update(100-Number(totalOperationRatio));
		}
	});
	
	setTimeout(getMachineInfo, 3000);
};

function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i);
		}
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			//marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};

function drawMachine(id, name, x, y, w, h, pic, status, i){
	var svgFile;
	if(status==null){
		svgFile = ".svg";
	}else{
		svgFile = "-" + status + ".svg";
	};
	
	machineList[i] = draw.image(imgPath + pic + svgFile).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	var text_color;
	if(status=="WAIT" || status=="NO-CONNECTION"){
		text_color = "#000000";
	}else{
		text_color = "#ffffff";
	};
	
	//idx, x, y, w, h, name
	printMachineName(i, x, y, w, h, name, text_color);
	
	//idx, machineId, w, h
	//setDraggable(i, id, w, h);
	
	//alarm timer
	/*if(status=="ALARM"){
		drawAlarmTimer(x, y, w, h, id);
	};*/
};

function drawAlarmTimer(x, y, w, h, id){
	var timerDiv = $("<div id='timer'>"+
								"<table>"+
								"<tr>"+
									"<tD>"+
										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD valign='middle'>"+
										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
									"</tD>"+
									"<tD>"+
										"<div id='second1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='second2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
								"</tr>"+
							"</table>"+
						"</div>");
	
	$(timerDiv).css({
		"position" : "absolute",
		//"width" : 200,
		"left" : x + (w/2) - (100),
		"top" : y + (h+5)
	});
	
	$("#odometerDiv").append(timerDiv);
};

function setDraggable(arrayIdx, id, w, h){
	//machineList[arrayIdx].draggable();
	
	machineList[arrayIdx].dragmove  = function(delta, evt){
		var x = machineList[arrayIdx].x();
		var y = machineList[arrayIdx].y();
		
		//setNamePos
		machineName[arrayIdx].x(x + (w/2) - 30);
		machineName[arrayIdx].y(y + (h/2) - 10);
		
		//DB Access
		setMachinePos(id, x, y);
	};
};

function setMachinePos(id, x, y){
	var url = ctxPath + "/svg/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};