var NS="http://www.w3.org/2000/svg";
var draw;
var background_img;
var marker;
var imgPath = "../images/DashBoard/";

var pieChart1;
var pieChart2;
var borderColor = "";
var shopId = 1;
var totalOperationRatio = 0;

$(function() {
	pieChart1 = $("#pieChart1").highcharts();
	pieChart2 = $("#pieChart2").highcharts();

	$("#title_main").css({
		left : window.innerWidth/2 - ($("#title_main").width()/2)
	});
	
	draw = SVG("svg");
	

	
	//getMarker();
	
	var timeStamp = new Date().getMilliseconds();
	
//	background_img = draw.image(imgPath+"Road.png?dummy=" + timeStamp).size(contentWidth * 0.9, contentHeight);
//	background_img.x(getElSize(450));
//	background_img.y(contentHeight/(targetHeight/100));
	
	
//	var logoText =  draw.text(function(add) {
//		  add.tspan("　　　성역 없는 ").fill("#313131");
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("전원이 참여하는 ").fill("#0005D5").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("　　나로 부터의 ").fill("#c00000").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  
//	});
//		
//	logoText.font({
//			'family':'Helvetica'
//			, size:     getElSize(75)
//			, anchor:   'right'
//			, leading:  '1.3em'
//		   ,'font-weight':'bolder',
//	});
//	
//	logoText.x(getElSize(2600));
//	logoText.y(contentHeight/(targetHeight/1730));
	
	getMachineInfo();
});

function alarmTimer(){
	setInterval(function(){
		if(s2==10){
    		s++;
    		$("#second1").html(s);
    		s2 = 0;	
    	};
    	
    	if(s==6){
    		s = 0;
    		m2++;
    		
    		$("#second1").html(s);
    		$("#minute2").html(m2);
    	};
    	
    	if(m2==10){
    		m2 = 0;
    		m++;
    		$("#minute1").html(m);
    	};
		
    	$("#second2").html(s2);
    	s2++;
    	
	},1000);
};

function redrawPieChart(){
	var url = ctxPath + "/svg/getMachineInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;

			noconn = [];
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			$(json).each(function(key, data){
				
				if(data.notUse==1){
					powerOffMachine--;
				};
				
				if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="IN-CYCLE-BLINK"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT" || data.lastChartStatus=="WAIT-NOBLINK"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM" || data.lastChartStatus=="ALARM-NOBLINK"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
					noconn.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"))
				}
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "green";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			
			//bodyNeonEffect(borderColor);
			
			$(".status_span").remove();
			var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
			$("#container").append(inCycleSpan);

			var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
			$("#container").append(waitSpan);

			var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
			$("#container").append(alarmSpan);

			var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
			$("#container").append(noConnSpan);
			
			var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
			$("#container").append(totalSpan);

			$("#inCycleSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "##1bbe1b"
			});

			$("#inCycleSpan").css({
				"top" : $("#status_chart").offset().top + getElSize(100),
				"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#inCycleSpan").width()/2)
			});
			
			$("#waitSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "#ffff04",
			});
			
			$("#waitSpan").css({
				"left" : $("#status_chart").offset().left + $("#status_chart").width() - getElSize(100) - $("#waitSpan").height(),
				"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#waitSpan").height()/2),
			});
			

			$("#alarmSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "#FF0000",
			});
			
			$("#alarmSpan").css({
				"top" : $("#status_chart").offset().top + $("#status_chart").height() - getElSize(100) - $("#alarmSpan").height(),
				"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#alarmSpan").width()/2)
			});

			$("#noConnSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "#CFD1D2"
			});
			
			$("#noConnSpan").css({
				"left" : $("#status_chart").offset().left + getElSize(100),
				"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#noConnSpan").height()/2),
			});

			$("#totalSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(60),
				"z-index" : 10,
				"color" : "#ffffff",
				"text-align" : "center"
			});

			$("#total_title").css({
				"font-size" : getElSize(45),
			});

			$("#totalSpan").css({
				"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#totalSpan").height()/2),
				"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#totalSpan").width()/2)  
			});
			
			
			
			
			$("svg").css({
				"z-index" : "100",
				"position" : "relative"
			});
		
			
			
			
		}
	});
};

/*
function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize, dvcId,cnt){
	
	//machineName[arrayIdx] = draw.text(nl2br(decodeURIComponent(name))).fill(color);
//	machineName[arrayIdx].font({
//		  family:   'Helvetica'
//		, size:     fontSize
//		, anchor:   'middle'
//		, leading:  '0em'
//	});
	
	if(arrayIdx==46){
		machineName[arrayIdx].click(function(){
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth()+1));
			var day = addZero(String(date.getDate()));
			var today = year + "-" + month + "-" + day;
			var url = ctxPath + "/device/getNightlyMachineStatus.do";
			var param = "stDate=" + $("#sDate").val() + 
						"&edDate=" + $("#sDate").val() +
						"&shopId=" + shopId;
			$("#tableSubTitle").html("기준 시간 : 18시 ~ 07시");
			if(!table_8){
				url = ctxPath + "/device/getNightlyMachineStatus_8.do";
				param = "stDate=" + today + 
						"&edDate=" + today + 
						"&shopId=" + shopId;
				$("#tableSubTitle").html("기준 시간 : 18시 ~ 08시");
			};
			
			$.ajax({
				url : url,
				data: param,
				dataType : "text",
				type : "post",
				success : function(data){
					var json = $.parseJSON(data);
					
					var tr = "<tr style='font-size: 30px; font-weight: bold;' class='tr' >" + 
								"<td class='td'>No</td>" + 
								"<td class='td'>설비명</td>" + 
								"<td class='td'>날짜</td>" + 
								"<td class='td'>정지시간</td>" + 
								"<td class='td'>상태</td>" + 
							"</tr>";
					csvData = "NO., 설비명, 날짜, 정지시간, 상태LINE";
					var status = "";
					var fontSize = getElSize(25);
					var backgroundColor = "";
					var fontColor = "";

					$(json).each(function (idx, data){
						if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
							status = "완료";
							fontColor = "black";
							backgroundColor = "yellow";
						}else if(data.status=='ALARM'){
							status = "중단";
							fontColor = "white";
							backgroundColor = "red";
						}else if(data.status=='IN-CYCLE'){
							status = "가동";
							fontColor = "white";
							backgroundColor = "green";
						};
						
						if(data.isOld=="OLD"){
							status = "미가동";
							fontColor = "white";
							backgroundColor = "black";
						};
						
						tr += "<tr class='contentTr'>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + (idx+1) + " </td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'> " + data.name + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopDate + "</td>" +
								"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopTime + "</td>" + 
								"<td style='border:1px solid white;font-Size:" + fontSize + "px; color:" + fontColor + "; background-color:" + backgroundColor + "'>" + status + "</td>" +
							"</tr>";
						csvData += (idx+1) + "," +
									data.name + "," + 
									data.stopDate + "," + 
									data.stopTime + "," + 
									status + "LINE";
					});
					
					if(json.length==0){
						tr += "<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
								"<td colspan='5'>없음</tb>" + 
							"<tr>";
					};
					
					$("#table").html(tr);
					$(".tr").css("font-size", getElSize(30));
					$(".td").css("padding", getElSize(10));
					
					$("#tableDiv").show();
					table_8 = !table_8;
					table_flag = true;
				}
			});
		});
	};
	
	
	
//	machineName[arrayIdx].dblclick(function(){
//		if(dvcId!=0){
//			window.sessionStorage.setItem("dvcId", dvcId);
//			window.sessionStorage.setItem("name", name);
//			
//			window.localStorage.setItem("dvcId",dvcId);
//			location.href="/Single_Chart_Status/index.do";
//		};
//	})
	
	var wMargin = 0;
	var hMargin = 0;
	
	if(name=="SET UP"){
		hMargin = contentHeight/(targetHeight/30);
	}else if(name.indexOf("WASHING")!=-1){
		wMargin = contentHeight/(targetHeight/10);
	}else if(name.indexOf("NHM")!=-1){
		wMargin = getElSize(-10);
	}

//	machineName[arrayIdx].x(x + (w/2) - wMargin);
//	machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/40)+hMargin));
//	
//	machineName[arrayIdx].leading(1);
	
	var name = "<div id='n" + dvcId + "' class='name'>" + name + "<br>" + cnt + "</div>";
	$("body").append(name)
	$("#n" +dvcId).css({
		"position" : "absolute",
		"background-color" : "rgba(0,0,0,0.6)",
		//"color" : color,
		"text-align" : "center",
		"color" : "white",
		"fontSize" : fontSize,
		"z-index" : 9
	});
	
	$("#n" +dvcId).css({
		"top" : $("#svg_td").offset().top + y + (h/2) - ($("#n" + dvcId).height()/2),
		"left" : $("#svg_td").offset().left + x + (w/2) - ($("#n" + dvcId).width()/2),
	}).dblclick(function(){
		if(dvcId!=0){
			window.sessionStorage.setItem("dvcId", dvcId);
			window.sessionStorage.setItem("name", name);
			window.sessionStorage.removeItem("date");
			
			window.localStorage.setItem("dvcId",dvcId);
			location.href="/Single_Chart_Status/index.do?lang=" + window.localStorage.getItem("lang");
		};
	});
};

*/



function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize, dvcId, notUse){

	
//	var name = "<span id='n" + dvcId + "' class='name'>" + name + "</span>";
//	
//    $("body").append(name)
//    
//    $("#n" +dvcId).css({
//        "position" : "absolute",
//        "color" : "white",
//        "fontSize" : fontSize,
//        "z-index" : 11111,
//        "weight": 'bold',
//         
//    });
//
//    $("#n" +dvcId).css({
////        "top" : $("#svg_td").offset().top + y + (h/2) - ($("#n" + dvcId).height()/2),
////        "left" : $("#svg_td").offset().left + x + (w/2) - ($("#n" + dvcId).width()/2),
//        "top" : $("#svg_td").offset().top + y + (h/2) - ($("#n" + dvcId).height()/2) - getElSize(380),
//        "left" : $("#svg_td").offset().left + x + (w/2) - ($("#n" + dvcId).width()/2),
//    });
//	

    
/* 원래 코드 */	
	machineName[arrayIdx] = draw.text(nl2br(name)).fill(color);
	machineName[arrayIdx].font({
		  family:   'Helvetica'
		, size:     fontSize
		, anchor:   'middle'
		, leading:  '0em'
		, weight: 'bold'
			,fill: color
// , background : "#FFF"
// , stroke: '#000'
//   , fill: '#fff'
// , bgground: '#fff'
// , foreground: '#fff'
	});
	
	
//	if(notUse==0){
		machineName[arrayIdx].dblclick(function(){
			if(dvcId!=0){
				window.sessionStorage.setItem("dvcId", dvcId);
				window.sessionStorage.setItem("name", name);
				window.sessionStorage.removeItem("date");
				
				window.localStorage.setItem("dvcId",dvcId);
				
				location.href="/Single_Chart_Status/index.do?lang=" + window.localStorage.getItem("lang");
			};
		})
//	}
	
	var wMargin = 0;
	var hMargin = 0;
	
	// 장비 명 출력 위치 조정 
	if(arrayIdx == 0){ // dvcId : 1
		machineName[arrayIdx].x(x + (w/2) - wMargin - 3);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) + 10);
	} else if (arrayIdx == 1){ // dvcId : 2
		machineName[arrayIdx].x(x + (w/2) - wMargin);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) + 10);
	} else if (arrayIdx == 3){ // dvcId : 4
		machineName[arrayIdx].x(x + (w/2) - wMargin + 6);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) + 7);
	} else if (arrayIdx == 4){ // dvcId : 5
		machineName[arrayIdx].x(x + (w/2) - wMargin + 6);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) + 7);
	} else if (arrayIdx == 6 || arrayIdx == 7){ // dvcId : 7, 8
		machineName[arrayIdx].x(x + (w/2) - wMargin + 12);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10));
	} else if (arrayIdx == 8){ // dvcId : 9
		machineName[arrayIdx].x(x + (w/2) - wMargin);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10) + 10);
	} else{
		machineName[arrayIdx].x(x + (w/2) - wMargin);
		machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10));
	}
	
	machineName[arrayIdx].leading(1);
};


function nl2br(value) { 
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;


function getMachineInfo(){
	var url = ctxPath + "/svg/getMachineInfo.do";
	var param = "shopId=" + shopId;
	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			//console.log(data)
			$(".name").remove();
			
			var json = data.machineList;
			newMachineStatus = new Array();
			machineProp = [];
			noconn = [];
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;

			operationTime = 0;
			
			$(".wifi").remove()
			
			
			
			
			$(json).each(function(key, data){
				var array = new Array();
				var machineColor = new Array();
				
//				console.log("data 확인 : " + data)
//				console.log("좌표 확인 ie_x : " + data.ie_x);
//				console.log("좌표 확인 ie_y : " + data.ie_y);
//				console.log("크기 확인 w : " + data.w);
//				console.log("크기 확인 h : " + data.h);
//				console.log("pic 확인 pic : " + data.pic);
//				
//				console.log("이름 확인 : " + data.name);
//				console.log("길 확인 : " + data.notUse);
				
				
				array.push(data.id);
				
				
				// jane test
				var data_nm = data.name
				
				if(data_nm.includes("%23")) {
					data_nm = data_nm.split("%23")[0] + "<br>" + "#" + data_nm.split("%23")[1]
				}
				//
				
				array.push(data_nm);
				
				//console.log("이름 확인 : " + data_nm);				
				//console.log("lastChartStatus : " + data.lastChartStatus);
				
//				array.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"));
				//array.push("M" + replaceAll(replaceAll(decodeURI(data.dvcId), "%23","#"),"%2F","/"));
				array.push(getElSize(data.x));
				array.push(getElSize(data.y));
				array.push(getElSize(data.w));
				array.push(getElSize(data.h));
				array.push(data.pic);
				array.push(data.lastChartStatus);
				array.push(data.dvcId);
				array.push(getElSize(data.fontSize));
				
				
				if(data.notUse==1){
					array.push("NOTUSE");
					 powerOffMachine--;
				}else{
					array.push(data.lastChartStatus);
				}
				array.push(data.notUse);
				
				
				operationTime += Number(data.operationTime);
				
				if(data.lastChartStatus=="IN-CYCLE" || data.lastChartStatus=="IN-CYCLE-BLINK"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT" || data.lastChartStatus=="WAIT-NOBLINK"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM" || data.lastChartStatus=="ALARM-NOBLINK" ){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
					noconn.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"))
				};
				
				
				var text_color;
				if(data.lastChartStatus=="WAIT" || data.lastChartStatus=="NO-CONNECTION"){
					text_color = "#000000";
				}else{
					text_color = "#ffffff";
				};
				
				
				//idx, x, y, w, h, name
				
			//	printMachineName(null, getElSize(data.x), getElSize(data.y), getElSize(data.w), getElSize(data.h), replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"), text_color, getElSize(data.fontSize), data.dvcId, replaceAll(replaceAll(decodeURI(data.cnt), "%23","#"),"%2F","/"));
				
				
				var img_width = getElSize(30);
				if(String(data.type).indexOf("IO") == -1 && data.type != null){
					var wifi = document.createElement("img");
					wifi.setAttribute("src", ctxPath + "/images/wifi.png");
					wifi.setAttribute("class", "wifi");
					
					wifi.style.cssText = "position : absolute" + 
										"; width : " + img_width + "px" +
										"; height : " + img_width + "px" +
										"; border-radius :50%" + 
										"; top: " + ($("#n" + data.dvcId).offset().top - img_width) + "px" + 
										"; z-index : " + 9 +
										"; left : " + $("#n" + data.dvcId).offset().left + "px"; 
					
					$("body").append(wifi)
					
					
					if(data.isChg){
						if(data.chgTy=="O"){
							var offset_chg = document.createElement("img");
							offset_chg.setAttribute("src", ctxPath + "/images/offset_chg.png");
							offset_chg.setAttribute("class", "wifi");
							
							offset_chg.style.cssText = "position : absolute" + 
												"; width : " + img_width + "px" +
												"; height : " + img_width + "px" +
												"; border-radius :50%" + 
												"; z-index : " + 9 +
												"; top: " + ($("#n" + data.dvcId).offset().top - img_width) + "px" +
												"; left : " + ($("#n" + data.dvcId).offset().left + $("#n" + data.dvcId).width() - img_width) + "px";
							
							$("body").append(offset_chg)
							
						}else{
							var condition_chg = document.createElement("img");
							condition_chg.setAttribute("src", ctxPath + "/images/condition_chg.png");
							condition_chg.setAttribute("class", "wifi");
							
							condition_chg.style.cssText = "position : absolute" + 
												"; width : " + img_width + "px" +
												"; height : " + img_width + "px" +
												"; border-radius :50%" + 
												"; z-index : " + 9 +
												"; top: " + ($("#n" + data.dvcId).offset().top - img_width) + "px" +
												"; left : " + ($("#n" + data.dvcId).offset().left + $("#n" + data.dvcId).width() - img_width) + "px";
							
							$("body").append(condition_chg)
						}
					}
				} 
				
				
				$(".status_span").remove();
				var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
				$("#container").append(inCycleSpan);

				var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
				$("#container").append(waitSpan);

				var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
				$("#container").append(alarmSpan);

				var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
				$("#container").append(noConnSpan);

				$("#inCycleSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "##1bbe1b"
				});

				$("#inCycleSpan").css({
					"top" : $("#status_chart").offset().top + getElSize(100),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#inCycleSpan").width()/2)					
				});
				
				$("#waitSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "yellow",
				});
				
				$("#waitSpan").css({
					"left" : $("#status_chart").offset().left + $("#status_chart").width() - getElSize(100) - $("#waitSpan").height(),
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#waitSpan").height()/2),
				});
				

				$("#alarmSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#FF0000",
				});
				
				$("#alarmSpan").css({
					"top" : $("#status_chart").offset().top + $("#status_chart").height() - getElSize(100) - $("#alarmSpan").height(),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#alarmSpan").width()/2)
				});

				$("#noConnSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#CFD1D2"
				});
				
				$("#noConnSpan").css({
					"left" : $("#status_chart").offset().left + getElSize(100),
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#noConnSpan").height()/2),
				});

				$("#totalSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(60),
					"z-index" : 10,
					"color" : "#ffffff",
					"text-align" : "center"
				});

				$("#total_title").css({
					"font-size" : getElSize(45),
				});

				$("#totalSpan").css({
					"top" : $("#status_chart").offset().top + ($("#status_chart").height()/2) - ($("#totalSpan").height()/2),
					"left" : $("#status_chart").offset().left + ($("#status_chart").width()/2) - ($("#totalSpan").width()/2)  
				});
				
				machineProp.push(array);
				/*machineProp.push("machine");
				machineProp.push("name");*/
				
				machineColor.push(data.id);
				machineColor.push(data.lastChartStatus);
				
				newMachineStatus.push(machineColor);
				
			//	printMachineName(null, getElSize(data.x), getElSize(data.y), getElSize(data.w), getElSize(data.h), replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"), text_color, getElSize(data.fontSize), data.dvcId, replaceAll(replaceAll(decodeURI(data.cnt), "%23","#"),"%2F","/"));
				
			});
			// wilson
//			var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
//			$("#container").append(totalSpan);
			
			
			if(!compare(machineStatus,newMachineStatus)){
				//clearInterval(border_interval);
				reDrawMachine();
				redrawPieChart();
			};
			
			if(first){
				for(var i = 0; i < machineProp.length; i++){
					//array = id, name, x, y, w, h, pic, status, idx, dvcId, fontSize
					drawMachine(machineProp[i][0],
							machineProp[i][1],
							machineProp[i][2],
							machineProp[i][3],
							machineProp[i][4],
							machineProp[i][5],
							machineProp[i][6],
							machineProp[i][7],
							i,
							machineProp[i][8],
							machineProp[i][9],
							machineProp[i][10]);
				};
				first = false;
			};
			
			//cal totalOperationRatio
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
			
//			var totalMachine = 0;
//			totalMachine += (inCycleMachine + waitMachine + alarmMachine);
//			var totalOperationRatio = Number(inCycleMachine / totalMachine * 100).toFixed(1);
////			pieChart1.series[0].data[0].update(Number(totalOperationRatio));
//			pieChart1.series[0].data[1].update(100-Number(totalOperationRatio));
			
			
		}, erorr : (e1,e2,e3)=>{
			console.log(e1)
			console.log(e2)
		}
	});
	
	
	// 3초(3000) 마다 페이지 갱신 : 장비 상태 갱신
	setTimeout(getMachineInfo, 3000);
};

function replaceAll(str, src, target){
	return str.split(src).join(target)
};

function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8],
					machineProp[i][9],
					machineProp[i][10]);
		}
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			//marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};



var table_8 = true


function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize, notUse){
	//alert(dvcId + ", " + name + ", " + notUse);
	var svgFile;
	var timeStamp = new Date().getMilliseconds();
	if(status==null){
		svgFile = ".svg";
	}else{
		//if(status=="ALARM" || name.indexOf("HFMS")!=-1) status = "IN-CYCLE";
		svgFile = "-" + status + ".svg";
	};
	
	//console.log(machineList);
	
	machineList[i] = draw.image(imgPath + pic + svgFile + "?dummy=" + timeStamp).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	
	console.log(machineList);
	//console.log("x, y : " + machineList[i].x(x) + ", " + x +  machineList[i].y(y) + ", " + y);
	
	var text_color;
//	if(status=="WAIT" || status=="NO-CONNECTION" || status=="IN-CYCLE"){
//		text_color = "#000000";
//	}else{
//		text_color = "#ffffff";
//	};
//	
//	if(status=="WAIT" || status=="NO-CONNECTION"){
//		text_color = "black";
//		
//	}else if(status=='ALARM'){
//		text_color = "white";
//		
//	}else if(status=='IN-CYCLE'){
//		text_color = "white";
//		
//	};
	
	// textColor
	if(status=="IN-CYCLE-BLICK" || status=="IN-CYCLE"){
		text_color = "#ffffff";
	}else{
		text_color = "#000000";
	};
	
	fontSize = getElSize(30);
	printMachineName(i, x, y, w, h, name, text_color, fontSize, dvcId, notUse);
	
	console.log("dvcId :" + dvcId + " , " + "notUse :" + notUse)
	
	if(notUse==0){
		machineList[i].dblclick(function(){
			if(dvcId!=0){
				window.localStorage.setItem("dvcId", dvcId);
				window.localStorage.setItem("name", name);
				
				// location.href=ctxPath +
				// "/chart/singleChartStatus.do?fromDashBoard=true";
				location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
			};
		})
	}
	
	//idx, machineId, w, h
//	setDraggable(i, id, w, h);
	

	machineList[i].dblclick(function(){
		if(dvcId!=0){
			window.sessionStorage.setItem("dvcId", dvcId);
			window.sessionStorage.setItem("name", name);
			window.sessionStorage.removeItem("date");
			
			window.localStorage.setItem("dvcId",dvcId);
			
			location.href="/Single_Chart_Status/index.do?lang=" + window.localStorage.getItem("lang");
		};
	})
	//alarm timer
	/*if(status=="ALARM"){
		drawAlarmTimer(x, y, w, h, id);
	};*/
};


function drawAlarmTimer(x, y, w, h, id){
	var timerDiv = $("<div id='timer'>"+
								"<table>"+
								"<tr>"+
									"<tD>"+
										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD valign='middle'>"+
										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
									"</tD>"+
									"<tD>"+
										"<div id='second1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='second2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
								"</tr>"+
							"</table>"+
						"</div>");
	
	$(timerDiv).css({
		"position" : "absolute",
		//"width" : 200,
		"left" : x + (w/2) - (100),
		"top" : y + (h+5)
	});
	
	$("#odometerDiv").append(timerDiv);
};

function setDraggable(arrayIdx, id, w, h){
	machineList[arrayIdx].draggable();
	
	machineList[arrayIdx].dragmove  = function(delta, evt){
		var x = machineList[arrayIdx].x();
		var y = machineList[arrayIdx].y();
		
		//setNamePos
//		machineName[arrayIdx].x(x + (w/2) - getElSize(30));
//		machineName[arrayIdx].y(y + (h/2) - getElSize(10));
		
		//DB Access
		setMachinePos(id, setElSize(x), setElSize(y));
	};
};

function setMachinePos(id, x, y){
	var url = ctxPath + "/svg/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};

















//var NS="http://www.w3.org/2000/svg";
//var draw;
//var background_img;
//var marker;
//var imgPath = "../images/DashBoard/";
//
//var borderColor = "";
//var shopId = 1;
//var totalOperationRatio = 0;
//var change = 0;
//$(function() {
//	draw = SVG("svg");
//	// getMarker();
//	
//	var timeStamp = new Date().getMilliseconds();	
//	getMachineInfo();
//});
//
//
//var getParameters = function (paramName) {
//    // 리턴값을 위한 변수 선언
//    var returnValue;
//
//    // 현재 URL 가져오기
//    var url = location.href;
//
//    // get 파라미터 값을 가져올 수 있는 ? 를 기점으로 slice 한 후 split 으로 나눔
//    var parameters = (url.slice(url.indexOf('?') + 1, url.length)).split('&');
//
//    // 나누어진 값의 비교를 통해 paramName 으로 요청된 데이터의 값만 return
//    for (var i = 0; i < parameters.length; i++) {
//        var varName = parameters[i].split('=')[0];
//        if (varName.toUpperCase() == paramName.toUpperCase()) {
//            returnValue = parameters[i].split('=')[1];
//            return decodeURIComponent(returnValue);
//        }
//    }
//};
//
//
//function alarmTimer(){
//	setInterval(function(){
//		if(s2==10){
//    		s++;
//    		$("#second1").html(s);
//    		s2 = 0;	
//    	};
//    	
//    	if(s==6){
//    		s = 0;
//    		m2++;
//    		
//    		$("#second1").html(s);
//    		$("#minute2").html(m2);
//    	};
//    	
//    	if(m2==10){
//    		m2 = 0;
//    		m++;
//    		$("#minute1").html(m);
//    	};
//		
//    	$("#second2").html(s2);
//    	s2++;
//    	
//	},1000);
//};
//
//function redrawPieChart(){
//	var url = ctxPath + "/svg/getMachineInfo2.do";
//	var param = "shopId=" + shopId;
//	
//	$.ajax({
//		url : url,
//		data : param,
//		type : "post",
//		dataType : "json",
//		success : function (data){
//			var json = data.machineList;
//			
//			noconn = [];
//			inCycleMachine = 0;
//			waitMachine = 0;
//			alarmMachine = 0;
//			powerOffMachine = 0;
//			
//			$(json).each(function(key, data){
//				if(data.notUse==1){
//				// powerOffMachine--;
//				};
//				
//				if(data.lastChartStatus=="IN-CYCLE"){
//					inCycleMachine++;
//				}else if(data.lastChartStatus=="WAIT"){
//					waitMachine++;
//				}else if(data.lastChartStatus=="ALARM"){
//					alarmMachine++;
//				}else if(data.lastChartStatus=="NO-CONNECTION" && data.notUse!=1){
//					powerOffMachine++;
//					noconn.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"))
//				};
//			});
//			
//			if(waitMachine==0 && alarmMachine==0){
//				borderColor = "green";
//			}else if(alarmMachine==0){
//				borderColor = "yellow";
//			}else if(alarmMachine!=0){
//				borderColor = "red";
//			};
//
//			
//			$(".status_span").remove();
//			var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
//			$("#container").append(inCycleSpan);
//			
//			var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
//			$("#container").append(waitSpan);
//			
//			var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
//			$("#container").append(alarmSpan);
//			
//			var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
//			$("#container").append(noConnSpan);
//			
//			$(".total").remove();
//			var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
//			$("#container").append(totalSpan);
//			
//			$("#inCycleSpan").css({
//				"position" : "absolute",
//				"font-size" : getElSize(95),
//				"z-index" : 10,
//				"color" : "#A3D800",
//				"top" : getElSize(1460) + marginHeight,
//				"left" : getElSize(1095) + marginWidth 
//			});
//			
//			$("#waitSpan").css({
//				"position" : "absolute",
//				"font-size" : getElSize(95),
//				"z-index" : 10,
//				"color" : "#FF9100",
//				"top" : getElSize(1700) + marginHeight,
//				"left" : getElSize(1340) + marginWidth
//			});
//			
//			$("#alarmSpan").css({
//				"position" : "absolute",
//				"font-size" : getElSize(95),
//				"z-index" : 10,
//				"color" : "#EC1C24",
//				"top" : getElSize(1920) + marginHeight,
//				"left" : getElSize(1095) + marginWidth 
//			});
//			
//			$("#noConnSpan").css({
//				"position" : "absolute",
//				"font-size" : getElSize(95),
//				"z-index" : 10,
//				"color" : "#CFD1D2",
//				"top" : getElSize(1700) + marginHeight,
//				"left" : getElSize(845) + marginWidth
//			});
//			
//			$("#totalSpan").css({
//				"position" : "absolute",
//				"font-size" : getElSize(70),
//				"z-index" : 10,
//				"color" : "#ffffff",
//				"top" : getElSize(1685) + marginHeight,
//				"left" : getElSize(1100) + marginWidth ,
//				"text-align" : "center"
//			});
//			
//			$("#total_title").css({
//				"font-size" : getElSize(45),
//			});
//			
//			
//			
//		}
//	});
//};
//
//function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize, dvcId, notUse){
//
//	/*
//	var name = "<span id='n" + dvcId + "' class='name'>" + name + "</span>";
//	
//    $("body").append(name)
//    
//    $("#n" +dvcId).css({
//        "position" : "absolute",
//        "color" : color,
//        "fontSize" : fontSize,
//        "background-color" : "white",
//        "z-index" : 11111
//    });
//
//    $("#n" +dvcId).css({
////        "top" : $("#svg_td").offset().top + y + (h/2) - ($("#n" + dvcId).height()/2),
////        "left" : $("#svg_td").offset().left + x + (w/2) - ($("#n" + dvcId).width()/2),
//        "top" : $("#svg_td").offset().top + y + (h/2) - ($("#n" + dvcId).height()/2) - 90,
//        "left" : $("#svg_td").offset().left + x + (w/2) - ($("#n" + dvcId).width()/2),
//    });
//	*/
//	
///* 원래 코드 */	
//	machineName[arrayIdx] = draw.text(nl2br(name)).fill(color);
//	machineName[arrayIdx].font({
//		  family:   'Helvetica'
//		, size:     fontSize
//		, anchor:   'middle'
//		, leading:  '0em'
//		, weight: 'bold'
//			
//// , background : "#FFF"
// , stroke: '#000'
//   , fill: '#fff'
//// , bgground: '#fff'
//// , foreground: '#fff'
//	});
//	
//	
//	if(notUse==0){
//		machineName[arrayIdx].dblclick(function(){
//			if(dvcId!=0){
//				window.localStorage.setItem("dvcId", dvcId);
//				window.localStorage.setItem("name", name);
//				
//				// location.href=ctxPath +
//				// "/chart/singleChartStatus.do?fromDashBoard=true";
//				location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
//			};
//		})
//	}
//	
//	var wMargin = 0;
//	var hMargin = 0;
//	
//	// 장비 명 출력 위치 조정 
//	machineName[arrayIdx].x(x + (w/2) - wMargin);
//	machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin) - (h/10));
//	
//	machineName[arrayIdx].leading(1);
//};
//
//function nl2br(value) { 
//	  return value.replace(/<br>/g, "\n");
//};
//
//var machineProp = new Array();
//var machineList = new Array();
//var machineName = new Array();
//var machineStatus = new Array();
//var newMachineStatus = new Array();
//var machineArray2 = new Array();
//var first = true;
//
//var preTimestap = 0;
//
//var maxTime = "";
//function getMachineInfo(){
//	var url = ctxPath + "/svg/getMachineInfo.do";
//	var date = new Date();
//	
//	var year = date.getFullYear();
//	var month = addZero(String(date.getMonth()+1));
//	var day = addZero(String(date.getDate()));
//	var today = year + "-" + month + "-" + day;
//	
//	var param = "shopId=" + shopId + 
//				"&startDateTime=" + today;
//	
//	$.ajax({
//		url : url,
//		data : param,
//		type : "post",
//		dataType : "json",
//		success : function (data){
//			console.log(data)
//			
//			var nowTime = new Date().getTime();
//			
//			// console.log((nowTime - preTimestap) / 1000);
//			preTimestap = nowTime
//			
//			
//			var json = data.machineList;
//			
//			newMachineStatus = new Array();
//			machineProp = [];
//			noconn = [];
//			inCycleMachine = 0;
//			waitMachine = 0;
//			alarmMachine = 0;
//			powerOffMachine = 0;
//
//			operationTime = 0;
//// maxTime = new Date(json[0].workDate.substr(0,4),
//// json[0].workDate.substr(5,2), json[0].workDate.substr(8,2),
//// json[0].workDate.substr(11,2),json[0].workDate.substr(14,2),
//// json[0].workDate.substr(17,2))
//			
//			$(json).each(function(key, data){
//				var time = data.workDate;
//// var uDate = new Date(time.substr(0,4), time.substr(5,2), time.substr(8,2),
//// time.substr(11,2),time.substr(14,2), time.substr(17,2))
////				
//// if(maxTime<uDate) maxTime = uDate
//				
//				var array = new Array();
//				var machineColor = new Array();
//				
//				array.push(data.id);
//				// array.push(replaceAll(replaceAll(decodeURI(data.name),
//				// "%23","#"),"%2F","/"));
//				// array.push(getElSize(data.x)-$("#svg").offset().left +
//				// marginWidth);
//				
//				
//				
//				// jane test
//				var data_nm = data.name
//				
//				if(data_nm.includes("#")) {
//					data_nm = data_nm.split("#")[0] + "<br>" + "#" + data_nm.split("#")[1]
//				}
//				//
//				
//				
//				
//				array.push(data_nm);
////				array.push(data.name);
//				array.push(getElSize(data.x));
//				array.push(getElSize(data.y));
//				array.push(getElSize(data.w * 0.8));
//				array.push(getElSize(data.h * 0.8));
//				array.push(data.pic);
//				
//				console.log(data);
//				console.log("좌표 확인 ie_x : " + data.ie_x);
//				console.log("좌표 확인 ie_y : " + data.ie_y);
//				console.log("크기 확인 w : " + data.w);
//				console.log("크기 확인 h : " + data.h);
//				console.log("pic 확인 pic : " + data.pic);
//				
//				console.log("이름 확인 : " + data.name);
//
//				
//				if(data.notUse==1){
//					array.push("NOTUSE");
//					 powerOffMachine--;
//				}else{
//					array.push(data.lastChartStatus);
//				}
//				array.push(data.dvcId);
//				array.push(getElSize(data.fontSize));
//				array.push(data.notUse);
//				
//				operationTime += Number(data.operationTime);
//				
//				if(data.lastChartStatus=="IN-CYCLE"){
//					inCycleMachine++;
//				}else if(data.lastChartStatus=="WAIT"){
//					waitMachine++;
//				}else if(data.lastChartStatus=="ALARM"){
//					alarmMachine++;
//				}else if(data.lastChartStatus=="NO-CONNECTION" && data.notUse!=1){
//					powerOffMachine++;
//					noconn.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"))
//				};
//				
//				
//				machineProp.push(array);
//				/*
//				 * machineProp.push("machine"); machineProp.push("name");
//				 */
//				
//				machineColor.push(data.id);
//				machineColor.push(data.lastChartStatus);
//				
//				newMachineStatus.push(machineColor);
//			});
//			
//			// maxTime = timeFormatter(maxTime)
//			
//			// $("#time").html("데이터수신시간 : " + maxTime);
//			
//			if(!compare(machineStatus,newMachineStatus)){
//				reDrawMachine();
//				redrawPieChart();
//			};
//			
//			if(first){
//				for(var i = 0; i < machineProp.length; i++){
//					// array = id, name, x, y, w, h, pic, status, idx, dvcId,
//					// fontSize, notUse
//					drawMachine(machineProp[i][0],
//							machineProp[i][1],
//							machineProp[i][2],
//							machineProp[i][3],
//							machineProp[i][4],
//							machineProp[i][5],
//							machineProp[i][6],
//							machineProp[i][7],
//							i,
//							machineProp[i][8],
//							machineProp[i][9],
//							machineProp[i][10]);
//				};
//				first = false;
//			};
//			
//			// cal totalOperationRatio
//			totalOperationRatio = 0;
//			for(var i = 0; i < machineArray2.length; i++){
//				totalOperationRatio += machineArray2[i][20];
//			};
//			
//		}
//	});
//	
//	// setTimeout(getMachineInfo, 3000);
//};
//
//function timeFormatter(time){
//	var year = time.getFullYear();
//	var month = addZero(String(time.getMonth()));
//	var day = addZero(String(time.getDate()));
//	var hour = time.getHours();
//	var minute = addZero(String(time.getMinutes()));
//	var second = addZero(String(time.getSeconds()));
//	
//	var ty;
//	if(hour>12){
//		ty = "오후"
//	}else{
//		ty = "오전"
//	}
//	
//	return today = year + "-" + month + "-" + day + " " + ty + " " + hour + " : " + minute + " : " + second
//	
//}
//
//function replaceAll(str, src, target){
//	return str.split(src).join(target)
//};
//
//function reDrawMachine(){
//	for(var i = 0; i < machineStatus.length; i++){
//		if(machineStatus[i][1]!=newMachineStatus[i][1]){
//			machineList[i].remove();
//			
//			// array = id, name, x, y, w, h, pic, status
//			
//			drawMachine(machineProp[i][0],
//					machineProp[i][1],
//					machineProp[i][2],
//					machineProp[i][3],
//					machineProp[i][4],
//					machineProp[i][5],
//					machineProp[i][6],
//					newMachineStatus[i][1],
//					i,
//					machineProp[i][8],
//					machineProp[i][9],
//					machineProp[i][10]);
//		}
//	};
//	
//	machineStatus = newMachineStatus;
//};
//
//function compare ( a, b ){
//	var type = typeof a, i, j;
//	 
//	if( type == "object" ){
//		if( a === null ){
//			return a === b;
//		}else if( Array.isArray(a) ){ // 배열인 경우
//			// 기본필터
//	      if( !Array.isArray(b) || a.length != b.length ) return false;
//	 
//	      // 요소를 순회하면서 재귀적으로 검증한다.
//	      for( i = 0, j = a.length ; i < j ; i++ ){
//	        if(!compare(a[i], b[i]))return false;
//	      	};
//	      return true;
//	    };
//	  };
//	 
//	  return a === b;
//};
//
//function getMarker(){
//	var url = ctxPath + "/svg/getMarker.do";
//	
//	$.ajax({
//		url : url,
//		type: "post",
//		dataType : "json",
//		success : function(data){
//			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
//			marker.x(data.x);
//			marker.y(data.y);
//			
//			
//			marker.draggable();
//			marker.dragmove  = function(delta, evt){
//				var id = data.id;
//				var x = marker.x();
//				var y = marker.y();
//				
//				// DB Access
//				setMachinePos(id, x, y);
//			};
//		}
//	});
//};
//
//function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize, notUse){
//	
//	
//	var svgFile;
//	var timeStamp = new Date().getMilliseconds();
//	if(status==null){
//		svgFile = ".svg";
//	}else{
//		svgFile = "-" + status + ".svg";
//	};
//	
//	console.log("id :" + dvcId + " ,w :" + w + " ,h :" +h +" ,x :" + x + " ,y :" + y);
//	// 박스 생성 공간부족해서 예외 DVC_ID 사이즈 줄임
//	if(/* dvcId==27 || dvcId==86 || dvcId==99 || */ dvcId==88 || dvcId==87 || dvcId==85 || dvcId==84 || dvcId==83 || dvcId==76 || dvcId==77 || dvcId==78){
//		machineList[i] = draw.image(imgPath + pic + "-" + status + "-if.svg" + "?dummy=" + timeStamp).size(w, h);
//		machineList[i].x(x);
//		machineList[i].y(y);
//	}else{
//		machineList[i] = draw.image(imgPath + pic + svgFile + "?dummy=" + timeStamp).size(w, h);
//		machineList[i].x(x);
//		machineList[i].y(y);
//	}
//	var text_color;
//	if(status=="WAIT" || status=="NO-CONNECTION" || status=="IN-CYCLE"){
//		text_color = "#000000";
//	}else{
//		text_color = "#ffffff";
//	};
//	
//	// idx, x, y, w, h, name
//	
//	if(getParameters('dvcId')==1){
//	// name = "I" + dvcId;
//	}else{
//	// name = name.substr(name.lastIndexOf("#")-1);
//	}
//	
//	console.log("이름? : " + name);
//		
//	
//	text_color = "black"
//	fontSize = 15
//	
//	printMachineName(i, x, y, w, h, name, text_color, fontSize, dvcId, notUse);
//	
//	// idx, machineId, w, h
//
//// setDraggable(i, id, w, h);
//	
//	if(notUse==0){
//		machineList[i].dblclick(function(){
//			if(dvcId!=0){
//				window.localStorage.setItem("dvcId", dvcId);
//				window.localStorage.setItem("name", name);
//				
//				// location.href=ctxPath +
//				// "/chart/singleChartStatus.do?fromDashBoard=true";
//				location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
//			};
//		})
//	}
//	
//	// alarm timer
//	if(status=="ALARM"){
//		drawAlarmTimer(x, y, w, h, id);
//	};
//};
//
//
//function drawAlarmTimer(x, y, w, h, id){
//	var timerDiv = $("<div id='timer'>"+
//								"<table>"+
//								"<tr>"+
//									"<tD>"+
//										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
//									"</tD>"+
//									"<tD>"+
//										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
//									"</tD>"+
//									"<tD valign='middle'>"+
//										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
//									"</tD>"+
//									"<tD>"+
//										"<div id='second1-" + id + "' class='odometer'>0</div>"+
//									"</tD>"+
//									"<tD>"+
//										"<div id='second2-" + id + "' class='odometer'>0</div>"+
//									"</tD>"+
//								"</tr>"+
//							"</table>"+
//						"</div>");
//	
//	$(timerDiv).css({
//		"position" : "absolute",
//		// "width" : 200,
//		"left" : x + (w/2) - (100),
//		"top" : y + (h+5)
//	});
//	
//	$("#odometerDiv").append(timerDiv);
//};
//
//function setDraggable(arrayIdx, id, w, h){
//	machineList[arrayIdx].draggable();
//	
//	machineList[arrayIdx].dragmove  = function(delta, evt){
//		var x = machineList[arrayIdx].x();
//		var y = machineList[arrayIdx].y();
//		
//		// setNamePos
//		machineName[arrayIdx].x(x + (w/2) - getElSize(30));
//		machineName[arrayIdx].y(y + (h/2) - getElSize(30));
//		
//		// DB Access
//		setMachinePos(id, setElSize(x - marginWidth + $("#svg").offset().left) , setElSize(y - marginHeight) );
//	};
//};
//
//function setMachinePos(id, x, y){
//	var url = ctxPath + "/svg/setMachinePos.do";
//	var param = "id=" + id + 
//					"&x=" + x + 
//					"&y=" + y;
//	
//	$.ajax({
//		url : url,
//		data : param,
//		type : "post",
//		success : function(data){}
//	});
//};
