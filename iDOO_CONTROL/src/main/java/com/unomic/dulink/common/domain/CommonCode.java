package com.unomic.dulink.common.domain;


public class CommonCode {
	//public static String myApiKey = "AIzaSyA0hVpizUS0REO6Xxu0EhYqNR8owmkVBww";
	//public static String myApiKey = "AIzaSyA2X_BiUXtFdM-nbCPgcPq88kbzagmP50Q";
	//public static String myApiKey = "AIzaSyDVXoY34Mnj9YB9g5RBQ6scwd6v_X1jShg";
	public static String myApiKey 							= "AIzaSyBeRxeV7CK4AibPFMTVGmBDd9XTwB3jc5Q";
	public static final String SERVER_IP 					= "106.240.234.114:8080";
	public static final String POST_URL 					= "/DULink/nfc/adapterPost.do";
	public static final String FILE_EXTEND 					= ".txt";
	
	//TARGET = "client"  is CNC Agent.
	//TARGET = "server" is UNOMIC Agent.
	public static final String TARGET 						= "server";
	
	
	// security cd
	public static String CODE_DVC_IOS						= "01300001";
	public static String CODE_DVC_ANDROID					= "01300002";
	
	public static String CODE_GCM_SUCCESS					= "01400001";
	public static String CODE_GCM_FAIL						= "01400002";
	//public static String CODE_GCM_FAIL_EMPTY_USER			= "01400002";
	//public static String CODE_GCM_FAIL_UNREG_USER			= "01400003";
	
	public static String MSG_IN_CYCLE						= "IN-CYCLE";
	public static String MSG_WAIT					  		= "WAIT";
	public static String MSG_ALARM							= "ALARM";
	public static String MSG_NO_CONNECTION					= "NO-CONNECTION";
	public static String MSG_COLOR_IN_CYCLE					= "colors[0]";
	public static String MSG_COLOR_WAIT					  	= "colors[1]";
	public static String MSG_COLOR_ALARM					= "colors[2]";
	public static String MSG_COLOR_NO_CONNECTION			= "colors[3]";
	public static String MSG_CHART_DIST						= "0.166666";
	public static String MSG_DATE_START						= "DATE_START";
	public static String MSG_STAT_INIT						= "STAT_INIT";
	public static String MSG_NO_ALARM						= "[{\"alarmMsg\":\"\",\"alarmCode\":\"\"},{\"alarmMsg\":\"\",\"alarmCode\":\"\"}]";
	public static String MSG_EXCEPTION_ALARM				= "[{\"alarmMsg\":\"EX2527 ATC MAGAZINE MANUAL SIDE TOOL LOCK ON ABNORMAL\",\"alarmCode\":\"0000\"},{\"alarmMsg\":\"EX2527 ATC MAGAZINE MANUAL SIDE TOOL LOCK ON ABNORMAL\",\"alarmCode\":\"0000\"}]";
	public static String MSG_HOUR_STD						= "18";
	public static String MSG_MIN_STD						= "00";
	public static String MSG_SDF_STD						= "yyyy-MM-dd "+MSG_HOUR_STD+":"+MSG_MIN_STD+":00";
	
	
	public static Integer CONNECT_TIMEOUT					= 3000;
	public static Integer READ_TIMEOUT						= 3000;
	public static Integer IOL_STATUS_LENGTH					= 4;
	
	public static String MSG_ADT_STATUS_SEND_REQ			= "SEND_REQ";
	public static String MSG_ADT_STATUS_NO_REQ				= "NO_REQ";
	
	public static String MSG_HTTP							= "http://";
	public static String MSG_AGT_URL						= ":5000/current";
	public static String MSG_IOL_URL						= "/getParam.cgi?DIStatus_00=?&DIStatus_01=?&DIStatus_02=?&DIStatus_03=?";
 
	
}

