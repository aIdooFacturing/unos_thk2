package com.unomic.dulink.common.domain;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;

public class CommonFunction {
	
	private static final Logger logger = LoggerFactory.getLogger(CommonFunction.class);
	
	public static String onlyNum(String str) {
		if ( str == null ) return "";

		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < str.length(); i++){
			if( Character.isDigit( str.charAt(i) ) ) {
				sb.append( str.charAt(i) );
			}
		}
		return sb.toString();
	}
	public static Boolean isNew(String str) {
		if ( str == null ) return false;

		StringBuffer sb = new StringBuffer();

		if( !Character.isDigit( str.charAt(0)) ) {
			return true;
		}else{
			return false;
		}
		

	}
	
	public static String mil2WorkDate(Long unixSec){
		
		DateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		
		sdfDateTime.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdfDate.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		Date crtDate = new Date(unixSec);
		Calendar crtCalendar = Calendar.getInstance();
		crtCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		
		try {
			crtCalendar.setTime(crtDate);
			
			Date stdDate = sdfDateTime.parse(CommonFunction.getStandardHourToday() );
			Calendar stdCalendar = Calendar.getInstance();
			stdCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
			stdCalendar.setTime(stdDate);
			
			if(crtCalendar.get(Calendar.HOUR_OF_DAY) * 60 + crtCalendar.get(Calendar.MINUTE) >= stdCalendar.get(Calendar.HOUR_OF_DAY) * 60 + stdCalendar.get(Calendar.MINUTE)){
				crtCalendar.add(Calendar.DATE, 1); // Adding 1 days
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String formattedDate = sdfDate.format(crtCalendar.getTime());
		
		return formattedDate;
	}
	
public static String dateTime2WorkDate(String dateTime){
		
		DateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		
		sdfDateTime.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdfDate.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		Date crtDate= new Date();
		try {
			crtDate = sdfDateTime.parse(dateTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Calendar crtCalendar = Calendar.getInstance();
		crtCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		crtCalendar.setTime(crtDate); // Now use today date.
		try {
		
			Date stdDate = sdfDateTime.parse(CommonFunction.getStandardHourToday() );
			Calendar stdCalendar = Calendar.getInstance();
			stdCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
			stdCalendar.setTime(stdDate);
		
			if(crtCalendar.get(Calendar.HOUR_OF_DAY) * 60 + crtCalendar.get(Calendar.MINUTE) >= stdCalendar.get(Calendar.HOUR_OF_DAY) * 60 + stdCalendar.get(Calendar.MINUTE)){
				crtCalendar.add(Calendar.DATE, 1); // Adding 1 days
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String formattedDate = sdfDate.format(crtCalendar.getTime());
		
		return formattedDate;		
	}
	
	public static String getTodayDateTime(){
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		Date date = new Date(System.currentTimeMillis());
		return sdf.format(date);
	}
	
	public static boolean isDateStart(String startTime,String endTime){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfSTD = new SimpleDateFormat(CommonCode.MSG_SDF_STD);
		//SimpleDateFormat sdfSTD = new SimpleDateFormat("yyyy-MM-dd 20:00:00");
		
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		sdfSTD.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		
		Date dateStd = new Date(System.currentTimeMillis());
		String timeStd = sdfSTD.format(dateStd);
		
		int compS=0;
		int compE=0;
		
		try {
			dateStd = sdf.parse(timeStd);

			Date dateStart = sdf.parse(startTime);
			compS = dateStart.compareTo(dateStd);
			
			Date dateEnd = sdf.parse(endTime);
			compE = dateEnd.compareTo(dateStd);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(compS < 0 && compE > 0){
			return true;
		}else{
			return false;
		}
	}
	
	public static String getStandardP1SecToday(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		
		Date date;
		Calendar c = Calendar.getInstance();
		try {
			date = sdf.parse(getStandardHourToday());
			c.setTime(date); // Now use today date.
			c.add(Calendar.SECOND, 1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sdf.format(c.getTime());
	}
	
	public static String getStandardHourToday(){
		SimpleDateFormat sdf = new SimpleDateFormat(CommonCode.MSG_SDF_STD);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		Date date = new Date(System.currentTimeMillis());
		return sdf.format(date);
	}
	
	public static String getStandardHour(String dateTime){
		SimpleDateFormat sdf = new SimpleDateFormat(CommonCode.MSG_SDF_STD);
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));	
		Date date = new Date(dateTime2Mil(dateTime));
		return sdf.format(date);
	}
	
	
	
	//변경 내용.
	//현재 시간 String 으로 입력. 테스트용이지만 나중에는 원하는 시간을 넣을 예정. 
	//그 
	public static String getChartStartTime(String dateTime){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		//Long lCDate = System.currentTimeMillis();
		Date stdDate;
		Calendar c = Calendar.getInstance();
		try {
			
			Date inputDate = sdf.parse(dateTime);
			Long lInuptDate = inputDate.getTime();
			
			stdDate = sdf.parse(getStandardHour(dateTime));
			c.setTime(stdDate); // Now use today date.
			
			Long lStdDate = stdDate.getTime();
			if(lInuptDate < lStdDate){
				c.add(Calendar.DATE, -1);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sdf.format(c.getTime());
	}
	
	public static String getStandardM1SecToday(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		
		Date date;
		Calendar c = Calendar.getInstance();
		try {
			date = sdf.parse(getStandardHourToday());
			c.setTime(date); // Now use today date.
			c.add(Calendar.SECOND, -1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sdf.format(c.getTime());
	}
	public static String unixTime2Datetime(Long unixSec){
		Date date = new Date(unixSec);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		String formattedDate = sdf.format(date);
		return formattedDate;
	}
	
	public static Long mtcDateTime2Mil(String dateTime){
		//String tmpStr = "2015-05-09T05:37:45Z";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		//sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		Calendar calendar = Calendar.getInstance();		
		try {
			Date date = sdf.parse(dateTime);
			calendar.setTime(date); // Now use today date.
			calendar.add(Calendar.HOUR, 9);
			calendar.getTimeInMillis();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return calendar.getTimeInMillis();
	}
	
	public static Long mtcDateTime2MilTemper(String dateTime){
		//String tmpStr = "2015-05-09T05:37:45Z";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		//sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));

		Calendar calendar = Calendar.getInstance();		
		try {
			Date date = sdf.parse(dateTime);
			calendar.setTime(date); // Now use today date.
			calendar.add(Calendar.HOUR, 9);
			calendar.getTimeInMillis();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return calendar.getTimeInMillis();
	}
	
	public static Long dateTime2Mil(String dateTime){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		Date date = new Date(System.currentTimeMillis());
		
		try {
			date = sdf.parse(dateTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return date.getTime();
	}
	
	public static int getMinDiff(String StartTime, String EndTime){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		
		Long difMin = 0L;
		try {
			Long longStart = sdf.parse(StartTime).getTime();
			Long longEnd = sdf.parse(EndTime).getTime();
			difMin = (longEnd - longStart) / 60000L;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return difMin.intValue();
	}
	public static int getSecDiff(String StartTime, String EndTime){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		
		Long difSec = 0L;
		try {
			Long longStart = sdf.parse(StartTime).getTime();
			Long longEnd = sdf.parse(EndTime).getTime();
			difSec = (longEnd - longStart) / 1000L;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return difSec.intValue();
	}
	
	public static String cutRight(String sText, int iTextLenth)
	{

		String sConvertText;
		
	    if (sText.length() < iTextLenth)
	    {
	        iTextLenth= sText.length();
	    }

	    sConvertText= sText.substring(0,sText.length() - iTextLenth);
	    return sConvertText;
	}
	
}

